"""
Unitary testing for AWS Bucket Wrapper module.

:author: Xavi

:date: 2018-10-26
"""

import logging
import mock
import unittest

from awsbucketwrapper import aws_bucket_wrapper


class TestAWSBucketWrapper(unittest.TestCase):
    HTTP_PROXY_IP_ADDRESS = "100.20.30.40:0123"
    get_logger_mock = mock.Mock()

    @classmethod
    def setUpClass(cls):
        """
        Global setUp
        """

        logging.basicConfig(level=logging.INFO)
        cls.bucket_name = "Bucket1"
        cls.valid_certificate_id = r"[certid_1]"
        cls.valid_folder_name = r"folder_1"
        cls.valid_imei = "0123456789876543210"
        cls.invalid_imei = 12345678987456321
        cls.valid_object_name = "123456789.98745"
        cls.valid_object_key1 = r"[certid_1]/folder_1"
        cls.valid_object_key2 = r"[certid_2]/folder_2"
        cls.valid_object_keys_list = [
            cls.valid_object_key1,
            cls.valid_object_key2
        ]
        cls.proxy = cls.HTTP_PROXY_IP_ADDRESS
        cls.windows_path = r"C:/Temp"
        cls.json_file_content = {
            "Header":
                {
                    "TCUID": 987456321,
                    "ConfigurationID": 456
                }
        }

    @mock.patch('logging.getLogger', get_logger_mock)
    def setUp(self):
        """
        Test setUp
        """
        self.path_args = []
        self.get_logger_mock.reset_mock()
        self.logger_mock = mock.Mock()
        self.get_logger_mock.return_value = self.logger_mock
        self.bucket_wrap = aws_bucket_wrapper.AWSBucketWrapper()
        self.object_summary_mock1 = mock.Mock()
        self.object_summary_mock2 = mock.Mock()

    def tearDown(self):
        """
        Test tearDown
        """
        pass

    def test_compose_bucket_key_full_path(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__compose_bucket_key'
        in the scenario that checks the "if" statement where a full path of a
        bucket object is provided.
        """
        composed_key = self.bucket_wrap._AWSBucketWrapper__compose_bucket_key(
            self.valid_certificate_id,
            self.valid_imei,
            self.valid_folder_name,
            self.valid_object_name
        )
        self.assertEquals(
            composed_key,
            self.valid_certificate_id + r"/" +
            self.valid_imei + r"/" +
            self.valid_folder_name + r"/" +
            self.valid_object_name + r"/"
        )

    def test_create_deletion_dictionary(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__create_deletion_dictionary'
        in the nominal and positive case.
        """
        deletion_dict = self.bucket_wrap._AWSBucketWrapper__create_deletion_dictionary(
            object_keys=self.valid_object_keys_list
        )
        self.assertEquals(
            deletion_dict,
            {
                "Objects":
                    [
                        {"Key": self.valid_object_keys_list[0]},
                        {"Key": self.valid_object_keys_list[1]},
                    ]
            }
        )

    def test_create_deletion_dictionary_input_keys_empty(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__create_deletion_dictionary'
        in case that the input list is empty, so there are no keys to put inside the
        deletion dictionary.
        """
        self.bucket_wrap._AWSBucketWrapper__create_deletion_dictionary(
            object_keys=[]
        )
        self.logger_mock.error.assert_called_once_with(
            "There are no object keys to delete.")

    def test_get_key_values_from_objectsummary(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__get_key_values_from_summary'
        in the positive and nominal case.
        """
        self.object_summary_mock1.key = self.valid_object_key1
        self.object_summary_mock2.key = self.valid_object_key2

        objects_summary = [self.object_summary_mock1, self.object_summary_mock2]

        object_keys = self.bucket_wrap._AWSBucketWrapper__get_key_values_from_summary(
            objects_summary=objects_summary
        )
        self.assertEquals(
            object_keys,
            [self.valid_object_key1, self.valid_object_key2]
        )

    def test_get_key_values_from_objectsummary_invalid_input(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__get_key_values_from_summary'
        in case that the input argument is not a list.
        """
        with self.assertRaises(TypeError) as err:
            self.bucket_wrap._AWSBucketWrapper__get_key_values_from_summary(
                objects_summary=123
            )
        self.assertEqual(
            str(err.exception),
            "A list of ObjectSummary objects shall be entered as input."
        )

    def test_get_all_messages_folder_key(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__get_all_messages_folder_key
        in the positive and nominal case.
        """
        folder_name = self.bucket_wrap._AWSBucketWrapper__get_all_messages_folder_key(
            key=self.valid_object_key1 + r"/" + self.valid_object_name
        )

        self.assertEquals(
            folder_name,
            self.valid_object_key1 + r"/all_messages/" + self.valid_object_name
        )

    def test_get_all_messages_folder_key_invalid_input(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__get_all_messages_folder_key
        in case that the input argument is not a string.
        """
        with self.assertRaises(TypeError) as err:
            self.bucket_wrap._AWSBucketWrapper__get_all_messages_folder_key(
                key=[]
            )
        self.assertEqual(
            str(err.exception),
            "An object key input shall be entered as a string."
        )

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_all_messages_folder_key')
    def test_move_objects_to_all_messages_folder(self, get_folder_mock):
        """
        This function tests
        'awsbucketwrapper.AWSBucketWrapper.__move_obj_to_all_msgs_folder
        in the positive and nominal case.
        """
        # Mocking the objectsummary that will be inside the input list argument
        self.object_summary_mock1.key = self.valid_object_key1
        self.object_summary_mock2.key = self.valid_object_key2
        objects_summary = [self.object_summary_mock1, self.object_summary_mock2]

        # Mocking the result for the __get_all_messages_folder_key function
        all_messages_folder_result = mock.Mock()
        get_folder_mock.return_value = all_messages_folder_result

        # Mocking the AWSBucketWrapper.__bucket attribute in order to call later
        # the function AWSBucketWrapper.__bucket.__move_obj_to_all_msgs_folder
        # function
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Start the test
        self.bucket_wrap._AWSBucketWrapper__move_obj_to_all_msgs_folder(
            object_keys_list=objects_summary
        )
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Key to move: " + self.valid_object_key1),
                mock.call("Key to move: " + self.valid_object_key2)
            ]
        )
        get_folder_mock.assert_has_calls(
            [
                mock.call(key=self.valid_object_key1),
                mock.call(key=self.valid_object_key2)
            ]
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.move_file_from_bucket. \
            assert_has_calls(
            [
                mock.call(
                    origin_bucket_name=self.bucket_wrap._AWSBucketWrapper__bucket_name,
                    origin_key_object=self.valid_object_key1,
                    destination_key=all_messages_folder_result
                ),
                mock.call(
                    origin_bucket_name=self.bucket_wrap._AWSBucketWrapper__bucket_name,
                    origin_key_object=self.valid_object_key2,
                    destination_key=all_messages_folder_result
                )
            ]
        )

    def test_get_object_name_from_key(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__get_object_name_from_key
        in the positive and nominal case.
        """
        object_name = self.bucket_wrap._AWSBucketWrapper__get_object_name_from_key(
            key=self.valid_object_key1 + r"/" + self.valid_object_name
        )
        self.assertEquals(object_name, self.valid_object_name)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_object_name_from_key')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_key_values_from_summary')
    def test_download_files_from_keys(
            self, get_key_from_summary_mock, get_object_name_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__download_files_from_keys
        in the positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.__move_obj_to_all_msgs_folder'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Creating the list input argument with the respective mocks that simulates the
        # objectssummary
        objects_summary = [self.object_summary_mock1, self.object_summary_mock2]

        # Giving a return value to the '__get_key_values_from_summary' function mock
        get_key_from_summary_mock.return_value = [
            self.valid_object_key1,
            self.valid_object_key2,
        ]
        # Giving a return value to the '__get_object_name_from_key' function mock
        get_object_name_mock.return_value = self.valid_object_name

        # Start the test
        self.bucket_wrap._AWSBucketWrapper__download_files_from_keys(
            objects_in_key_list=objects_summary,
            destination_folder=self.windows_path
        )
        get_key_from_summary_mock.assert_called_once_with(objects_summary)
        get_object_name_mock.assert_has_calls(
            [
                mock.call(self.valid_object_key1),
                mock.call(self.valid_object_key2)
            ]
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.download_file_from_bucket. \
            assert_has_calls(
            [
                mock.call(
                    file_key=self.valid_object_key1,
                    destination_path=self.windows_path + r"/" + self.valid_object_name
                ),
                mock.call(
                    file_key=self.valid_object_key2,
                    destination_path=self.windows_path + r"/" + self.valid_object_name
                )
            ]
        )

    @mock.patch("__builtin__.open", new_callable=mock.mock_open)
    @mock.patch("json.load")
    def test_retrieve_msg_from_json_file(self, json_load_mock, mock_file):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__retrieve_msg_from_json_file
        in the positive and nominal case.
        """
        # Giving a return value to the json.load() function call
        json_load_mock.return_value = self.json_file_content

        # Start the test
        json_dictionary_result = self.bucket_wrap. \
            _AWSBucketWrapper__retrieve_msg_from_json_file(
            folder_path=self.windows_path,
            filename=self.valid_object_name)

        mock_file.assert_called_once_with(
            self.windows_path + r"/" + self.valid_object_name
        )
        json_load_mock.assert_called_once()
        self.assertEquals(json_dictionary_result, self.json_file_content)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__retrieve_msg_from_json_file')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_object_name_from_key')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_key_values_from_summary')
    def test_retrieve_all_messages(
            self, get_key_from_summary_mock, get_object_name_mock,
            get_message_from_json_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.__retrieve_all_messages
        in the positive and nominal case.
        """
        # Creating the list input argument with the respective mocks that simulates the
        # objectssummary
        objects_summary = [self.object_summary_mock1, self.object_summary_mock2]

        # Giving a return value to the '__get_key_values_from_summary' function mock
        get_key_from_summary_mock.return_value = [
            self.valid_object_key1,
            self.valid_object_key2,
        ]
        # Giving a return value to the '__get_object_name_from_key' function mock
        get_object_name_mock.return_value = self.valid_object_name

        # Giving a return value to the '__retrieve_msg_from_json_file' function mock
        json_messge_mock = mock.Mock()
        get_message_from_json_mock.return_value = json_messge_mock

        # Start the test
        messages_list = self.bucket_wrap._AWSBucketWrapper__retrieve_all_messages(
            objects_in_key_list=objects_summary,
            destination_folder=self.windows_path
        )

        get_key_from_summary_mock.assert_called_once_with(
            objects_summary=objects_summary)

        get_object_name_mock.assert_has_calls(
            [
                mock.call(key=self.valid_object_key1),
                mock.call(key=self.valid_object_key2)
            ]
        )
        get_message_from_json_mock.assert_has_calls(
            [
                mock.call(folder_path=self.windows_path, filename=self.valid_object_name),
                mock.call(folder_path=self.windows_path, filename=self.valid_object_name)
            ]
        )
        self.assertEquals(messages_list, [json_messge_mock, json_messge_mock])

    def test_connect_to_bucket(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.connect_to_bucket
        in the positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.__move_obj_to_all_msgs_folder'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Start the test
        self.bucket_wrap.connect_to_bucket(
            bucket_name=self.bucket_name,
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2,
            http_proxy=self.proxy,
            https_proxy=self.proxy
        )
        self.assertEquals(
            self.bucket_wrap._AWSBucketWrapper__bucket_name,
            self.bucket_name
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.set_session_keys. \
            assert_called_once_with(
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.configure_proxy_settings. \
            assert_called_once_with(
            http_proxy=self.proxy,
            https_proxy=self.proxy
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.connect_to_bucket. \
            assert_called_once_with(bucket_name=self.bucket_name)

    def test_connect_to_bucket_with_only_one_proxy_input(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.connect_to_bucket
        in the positive case where in the input has only been provided one proxy
        setting.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.__move_obj_to_all_msgs_folder'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Start the test
        self.bucket_wrap.connect_to_bucket(
            bucket_name=self.bucket_name,
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2,
            https_proxy=self.proxy
        )
        self.assertEquals(
            self.bucket_wrap._AWSBucketWrapper__bucket_name,
            self.bucket_name
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.set_session_keys. \
            assert_called_once_with(
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.configure_proxy_settings. \
            assert_called_once_with(
            http_proxy="",
            https_proxy=self.proxy
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.connect_to_bucket. \
            assert_called_once_with(bucket_name=self.bucket_name)

    def test_connect_to_bucket_without_proxy_input(self):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.connect_to_bucket
        in the negative case that proxy settings haven't been provided.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.__move_obj_to_all_msgs_folder'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Start the test
        self.bucket_wrap.connect_to_bucket(
            bucket_name=self.bucket_name,
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2,
        )
        self.assertEquals(
            self.bucket_wrap._AWSBucketWrapper__bucket_name,
            self.bucket_name
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.set_session_keys. \
            assert_called_once_with(
            access_key=self.valid_object_key1,
            secret_key=self.valid_object_key2
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.configure_proxy_settings. \
            assert_not_called()
        self.bucket_wrap._AWSBucketWrapper__bucket.connect_to_bucket. \
            assert_called_once_with(bucket_name=self.bucket_name)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__move_obj_to_all_msgs_folder')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_clear_all_messages(self, compose_bucket_key_mock, move_objects_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.clear_all_messages
        in the positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Simulated key result
        composed_key = self.valid_certificate_id + self.valid_folder_name + self.valid_imei
        compose_bucket_key_mock.return_value = composed_key

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        filter_generator = (self.valid_object_key1, self.valid_object_key2)
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = filter_generator

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        self.bucket_wrap.clear_all_messages(
            self.path_args
        )
        compose_bucket_key_mock.assert_called_once_with(
            *self.path_args
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_called_once_with(
            prefix_filter=composed_key,
            delimiter_subfolders=True
        )
        move_objects_mock.assert_called_once_with(
            object_keys_list=list(filter_generator)
        )

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__move_obj_to_all_msgs_folder')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_clear_all_messages_already_created_filter(self, compose_key_mock, move_objects_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.clear_all_messages
        in the positive case that the result of the filter_objects is empty.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Simulated key result
        composed_key = self.valid_certificate_id + self.valid_folder_name + self.valid_imei
        compose_key_mock.return_value = composed_key

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = []

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        self.bucket_wrap.clear_all_messages(
            self.path_args,
            already_filtered_objects_list=self.valid_object_keys_list
        )
        compose_key_mock.assert_called_once_with(*self.path_args)

        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_not_called()
        self.logger_mock.warn.assert_not_called()
        move_objects_mock.assert_called_once_with(object_keys_list=self.valid_object_keys_list)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__move_obj_to_all_msgs_folder')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_clear_all_messages_already_created_key(self, compose_key_mock, move_objects_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.clear_all_messages
        in the positive case that the result of the filter_objects is empty.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = []

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        self.bucket_wrap.clear_all_messages(
            self.path_args,
            already_retrieved_key=self.valid_object_key1,
            already_filtered_objects_list=self.valid_object_keys_list
        )
        compose_key_mock.assert_not_called()

        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_not_called()
        self.logger_mock.warn.assert_not_called()
        move_objects_mock.assert_called_once_with(object_keys_list=self.valid_object_keys_list)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        'clear_all_messages')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__retrieve_all_messages')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__download_files_from_keys')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_get_incoming_messages(
            self, compose_key_mock, download_files_mock, retrieve_messages_mock,
            clear_messages_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.get_incoming_messages
        in the positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Simulated key result
        composed_key = self.valid_certificate_id + self.valid_folder_name + self.valid_imei
        compose_key_mock.return_value = composed_key

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        filtered_object_keys_mock = [self.valid_object_key1, self.valid_object_key2]
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = filtered_object_keys_mock

        # Mocking the result of the AWSBucketWrapper.__bucket.__retrieve_all_messages'
        retrieve_messages_mock.return_value = self.valid_object_keys_list

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        messages_list_response = self.bucket_wrap.get_incoming_messages(
            self.path_args,
            self.windows_path
        )
        compose_key_mock.assert_called_once_with(*self.path_args)
        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_called_once_with(
                prefix_filter=composed_key,
                delimiter_subfolders=True
            )
        download_files_mock.assert_called_once_with(
            objects_in_key_list=filtered_object_keys_mock,
            destination_folder=self.windows_path
        )
        retrieve_messages_mock.assert_called_once_with(
            objects_in_key_list=filtered_object_keys_mock,
            destination_folder=self.windows_path
        )
        clear_messages_mock.assert_called_once_with(
            path_args=self.path_args,
            already_retrieved_key=composed_key,
            already_filtered_objects_list=filtered_object_keys_mock

        )
        self.assertEquals(messages_list_response, self.valid_object_keys_list)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        'clear_all_messages')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__retrieve_all_messages')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__download_files_from_keys')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_get_next_message(
            self, compose_key_mock, download_files_mock, retrieve_messages_mock,
            clear_messages_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.get_net_message
        in the positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Simulated key result
        composed_key = self.valid_certificate_id + self.valid_folder_name + self.valid_imei
        compose_key_mock.return_value = composed_key

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        filtered_object_keys_mock = [self.valid_object_key1, self.valid_object_key2]
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = filtered_object_keys_mock

        # Mocking the result of the AWSBucketWrapper.__bucket.__retrieve_all_messages'
        retrieve_messages_mock.return_value = self.valid_object_keys_list

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        messages_list_response = self.bucket_wrap.get_oldest_message(
            self.path_args,
            self.windows_path
        )
        compose_key_mock.assert_called_once_with(*self.path_args)
        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_called_once_with(
                prefix_filter=composed_key,
                delimiter_subfolders=True
            )
        last_object_key = [filtered_object_keys_mock[0]]
        download_files_mock.assert_called_once_with(
            objects_in_key_list=last_object_key,
            destination_folder=self.windows_path
        )
        retrieve_messages_mock.assert_called_once_with(
            objects_in_key_list=last_object_key,
            destination_folder=self.windows_path
        )
        clear_messages_mock.assert_called_once_with(
            path_args=self.path_args,
            already_retrieved_key=composed_key,
            already_filtered_objects_list=last_object_key
        )
        self.assertEquals(messages_list_response, self.valid_object_keys_list)

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_key_values_from_summary')
    def test_ls(self, get_key_values_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.func_ls in the
        positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        self.bucket_wrap._AWSBucketWrapper__bucket.summary_all_objects_from_bucket. \
            return_value = self.valid_object_keys_list

        # Mocking the result of 'AWSBucketWrapper.__get_key_values_from_summary'
        # so it will be a list that contain two objectsummary keys which can be accessed
        # with their python property 'key' in order to extrat their real 'key' value
        get_key_values_mock.return_value = [self.valid_object_key1, self.valid_object_key2]

        # Start the test
        summary_key_list_result = self.bucket_wrap.func_ls()
        self.bucket_wrap._AWSBucketWrapper__bucket.summary_all_objects_from_bucket. \
            assert_called_once()

        get_key_values_mock.assert_called_once_with(self.valid_object_keys_list)

        self.logger_mock.info.assert_has_calls(
            [
                mock.call("func_ls AWS_S3_Bucket: "),
                mock.call("    " + self.valid_object_key1),
                mock.call("    " + self.valid_object_key2)
            ]
        )
        self.assertEquals(
            summary_key_list_result,
            [
                self.valid_object_key1,
                self.valid_object_key2
            ]
        )

    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__get_key_values_from_summary')
    @mock.patch.object(
        aws_bucket_wrapper.AWSBucketWrapper,
        '_AWSBucketWrapper__compose_bucket_key')
    def test_ls_grep(self, compose_key_mock, get_keys_from_summary_mock):
        """
        This function tests 'awsbucketwrapper.AWSBucketWrapper.ls_grep in the
        positive and nominal case.
        """
        # Mocking the 'AWSBucketWrapper.__bucket' attribute in order to call later
        # the function 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        bucket_mock = mock.Mock()
        self.bucket_wrap._AWSBucketWrapper__bucket = bucket_mock

        # Simulated key result
        composed_key = self.valid_certificate_id + self.valid_folder_name + self.valid_imei
        compose_key_mock.return_value = composed_key

        # Mocking the result of the 'AWSBucketWrapper.__bucket.filtered_objects_list_mock'
        # as it returns a generator that contains many ObjectSummary objects
        filtered_object_keys_mock = [self.valid_object_key1, self.valid_object_key2]
        self.bucket_wrap._AWSBucketWrapper__bucket. \
            filter_objects_by_key_on_bucket.return_value = filtered_object_keys_mock

        # Mocking the result of 'AWSBucketWrapper.__get_key_values_from_summary'
        # so it will be a list that contain two objectsummary keys which can be accessed
        # with their python property 'key' in order to extrat their real 'key' value
        get_keys_from_summary_mock.return_value = [
            self.valid_object_key1,
            self.valid_object_key2
        ]

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_folder_name)
        self.path_args.append(self.invalid_imei)

        # Start the test
        summary_key_list_result = self.bucket_wrap.ls_grep(
            self.path_args
        )
        compose_key_mock.assert_called_once_with(
            *self.path_args
        )
        self.bucket_wrap._AWSBucketWrapper__bucket.filter_objects_by_key_on_bucket. \
            assert_called_once_with(
            prefix_filter=composed_key,
            delimiter_subfolders=False
        )
        get_keys_from_summary_mock.assert_called_once_with(filtered_object_keys_mock)
        self.logger_mock.info.assert_has_calls(
            [mock.call('func_ls grep AWS_S3_Bucket for [certid_1]folder_10123456789876543210: '),
             mock.call('    [certid_1]/folder_1'),
             mock.call('    [certid_2]/folder_2')]
        )
        self.assertEquals(
            summary_key_list_result,
            [
                self.valid_object_key1,
                self.valid_object_key2
            ]
        )
