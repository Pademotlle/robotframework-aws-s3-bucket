"""
Wrapper used to perform specific actions at user level that involves a connection and the use of
the exposed AWS S3
Bucket features.

:author: Xavi


:date: 2018-10-24
"""

import logging
import json
import re

from awsbucket import aws_bucket


class AWSBucketWrapperBaseError(Exception):
    """
    Base module error
    """


class AWSNoKeysFoundError(AWSBucketWrapperBaseError):
    """
    No AWS Bucket object keys were found.
    """


class AWSBucketWrapper(object):
    """
    Class that wraps all features from awsbucket.aws_bucket module in a user-friendly format.
    """

    __ALL_MSGS_FOLDER_KEY_PATTERN = r"\w+\.\w+$"

    def __init__(self):
        self.__logger = logging.getLogger(__name__)
        self.__bucket = aws_bucket.AWSBucket()
        self.__bucket_name = ""
        self.__compiled_pattern = re.compile(self.__ALL_MSGS_FOLDER_KEY_PATTERN)
        self.__empty_list = []

    @staticmethod
    def __compose_bucket_key(*args_in_path):
        """
        This function composes the key used to get Bucket objects by concatenating the
        input arguments.

        Args:
            *args_in_path (tuple): Each input parameter entered as a "*args" will be treated as
                                   a path element. So each one shall be entered in string format.
        Return:
            string: Concatenation of all the input arguments in the defined order.
        """
        path = ""
        for arg in args_in_path:
            path += arg + r"/"
        return path

    def __create_deletion_dictionary(self, object_keys):
        """
        This function creates a deletion dictionary with all keys in order to target all files
        that want to be deleted.

        Args:
            object_keys (list): A list filled with many keys of the objects that will be deleted.
        Return:
            dictionary: A dictionary with the needed composition in order to delete objects from
                        a AWS S3 Bucket key.
        """
        if object_keys:
            delete_dict = {"Objects": []}
            for key in object_keys:
                delete_dict["Objects"].append({"Key": key})
            return delete_dict
        else:
            self.__logger.error("There are no object keys to delete.")

    @staticmethod
    def __get_key_values_from_summary(objects_summary):
        """
        This function retrieves all keys from a list of SummaryObjects into a list format.

        Args:
            objects_summary (list): A list filled with many S3.ObjectSummary objects that
                                    contain the desired key of the object.
        Return:
            list: List with all keys extracted from the S3.SumumaryObjects
        """
        object_keys = []
        if isinstance(objects_summary, list):
            for obj_sum in objects_summary:
                object_keys.append(obj_sum.key)
        else:
            raise TypeError("A list of ObjectSummary objects shall be entered as input.")
        return object_keys

    def __get_all_messages_folder_key(self, key):
        """
        This function retrieves the messages_folder path for the given key. It just only adds
        the path 'all_messages' at the end of the key, but just before the object_name.

        Args:
            key (str): AWS S3 Bucket key that will be used to define the path of the folder
                       'all_messages'. This folder will be used in order to place all consumed
                       messages.
        """
        if isinstance(key, (str, unicode)):
            end_key_name = self.__get_object_name_from_key(key)
            return re.sub(
                self.__ALL_MSGS_FOLDER_KEY_PATTERN,
                r"all_messages/" + end_key_name,
                key
            )
        else:
            raise TypeError("An object key input shall be entered as a string.")

    def __move_obj_to_all_msgs_folder(self, object_keys_list):
        """
        This function moves all objects inside a key to a 'all_messages' folder. This folder will
        be used in order to place all consumed messages.

        Args:
            object_keys_list (list): List that contains all keys that defines the placement of the
                                     objects that want to be moved.
        """
        for key_obj in object_keys_list:
            err_msg = "Key to move: %s" % key_obj.key
            self.__logger.info(err_msg)

            all_messages_file_key = self.__get_all_messages_folder_key(key=key_obj.key)

            self.__bucket.move_file_from_bucket(
                origin_bucket_name=self.__bucket_name,
                origin_key_object=key_obj.key,
                destination_key=all_messages_file_key
            )

    def __get_object_name_from_key(self, key):
        """
        This function retrieves the file name from the complete given key.

        Args:
            key (str): Gets only the file_name part that appears at the end of the given key.
                       i.e.: certificate/folder/imei/file_name --> file_name
        Return:
            string: file_name retrieved from the given key.
        """
        return self.__compiled_pattern.search(key).group(0)

    def __download_files_from_keys(self, objects_in_key_list, destination_folder):
        """
        This function downloads all files from the given keys into the given destination folder.

        Args:
            objects_in_key_list (list): List that contains many S3.ObjectSummary objects that
            contain
                                   the needed keys to interact with the objects that are inside
                                   the AWS S3 Bucket.
            destination_folder (str): Path folder where the downloaded files will be placed.
                                      i.e.: C:/Temp (in Windows)
        """
        object_keys_list = self.__get_key_values_from_summary(objects_in_key_list)
        for key in object_keys_list:
            object_name = self.__get_object_name_from_key(key)
            path_folder = destination_folder + r"/" + object_name
            self.__bucket.download_file_from_bucket(
                file_key=str(key),
                destination_path=str(path_folder)
            )

    @staticmethod
    def __retrieve_msg_from_json_file(folder_path, filename):
        """
        This function retrieves a JSON from an existing file in the given path.

        Args:
            filename (str): Filename of the target JSON file.
            folder_path (str): Path where is placed the target JSON file.
        Return:
            dictionary: All JSON file content loaded as a Python dictionary structure.
        """
        with open(folder_path + r"/" + filename) as json_data_file:
            json_dict = json.load(json_data_file)
            return json_dict

    def __retrieve_all_messages(self, objects_in_key_list, destination_folder):
        """
        This function retrieves and lists all messages that have been downloaded from
        Bucket and placed in the destination path.

        Args:
            objects_in_key_list (list): List filled with S3.ObjectSummary objects that contain
                                   the key and name of all JSON messages files.
            destination_folder (str): Path where are placed all messages downloaded from AWS S3
            Bucket.
        Return:
            list: List of python dictionaries that contain all downloaded messages info.
        """
        messages_list = []
        object_keys_list = self.__get_key_values_from_summary(
            objects_summary=objects_in_key_list)

        for file_key in object_keys_list:
            file_name = self.__get_object_name_from_key(key=file_key)

            messages_list.append(self.__retrieve_msg_from_json_file(
                folder_path=destination_folder,
                filename=file_name))
        return messages_list

    def connect_to_bucket(self, bucket_name, access_key, secret_key, http_proxy="",
                          https_proxy=""):
        """
        This function establishes a connection to the desired AWS S3 Bucket.

        Args:
            bucket_name (str): AWS S3 Bucket name
            access_key (str): Provided key that grants access to the AWS S3 Bucket.
            secret_key (str): Provided key that grants access to the AWS S3 Bucket.
            http_proxy (str): IP address for a possible proxy server. i.e: "10.23.45.67:8901"
            https_proxy (str): IP address for a possible proxy server. i.e: "10.23.45.67:8901"
        """
        self.__bucket_name = bucket_name
        self.__bucket.set_session_keys(
            access_key=access_key,
            secret_key=secret_key
        )
        if http_proxy or https_proxy:
            self.__bucket.configure_proxy_settings(
                http_proxy=http_proxy,
                https_proxy=https_proxy
            )
        self.__bucket.connect_to_bucket(bucket_name=bucket_name)

    def clear_all_messages(self, path_args, already_filtered_objects_list=None, already_retrieved_key=None):
        """
        This function moves all existing objects inside the given Bucket path (key) into
        the folder "all_messages" that also exists inside the same given Bucket directory.

        Args:
            path_args (list): List that contains all Bucket path (key) elements in string format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3
            already_retrieved_key (string): In case that the S3 bucket key with the path elements is already
                                            retrieved, then is not necessary to generate it again.
            already_filtered_objects_list (list): Optional. If it's not empty, it means that
                                            previously the object list was already retrieved
        """
        if not already_retrieved_key:
            key = self.__compose_bucket_key(*path_args)
        else:
            key = already_retrieved_key

        if not already_filtered_objects_list:
            filtered_objects_list = list(self.__bucket.filter_objects_by_key_on_bucket(
                prefix_filter=key,
                delimiter_subfolders=True))
            if not filtered_objects_list:
                self.__logger.info("There are no objects in the given key. Folder is empty.")

            self.__move_obj_to_all_msgs_folder(
                object_keys_list=filtered_objects_list
            )
        else:
            self.__move_obj_to_all_msgs_folder(
                object_keys_list=already_filtered_objects_list
            )

    def get_incoming_messages(self, path_args, destination_folder):
        """
        This function retrieves last messages for a concrete TCU (a concrete bucket path) from AWS
        S3 Bucket source.

        Args:
            path_args (list): List that contains all path elements entered in string format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3
            destination_folder (str): Host path of the folder that will be placed all
                                      downloaded files.
        """
        key = self.__compose_bucket_key(*path_args)

        objects_in_key = self.__bucket.filter_objects_by_key_on_bucket(
            prefix_filter=key,
            delimiter_subfolders=True
        )
        self.__download_files_from_keys(
            objects_in_key_list=objects_in_key,
            destination_folder=destination_folder
        )
        messages_list = self.__retrieve_all_messages(
            objects_in_key_list=objects_in_key,
            destination_folder=destination_folder
        )
        self.clear_all_messages(
            path_args=path_args,
            already_retrieved_key=key,
            already_filtered_objects_list=objects_in_key
        )
        return messages_list

    def get_oldest_message(self, path_args, destination_folder):
        """
        This function retrieves oldest message for a concrete TCU (a concrete bucket path)
        from AWS S3 Bucket source.

        Args:
            path_args (list): List that contains all path elements entered in string format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3
            destination_folder (str): Host path of the folder that will be placed all
                                      downloaded files.
        """
        key = self.__compose_bucket_key(*path_args)

        objects_in_key = self.__bucket.filter_objects_by_key_on_bucket(
            prefix_filter=key,
            delimiter_subfolders=True
        )
        if len(objects_in_key) < 1:
            raise Exception("Message not received!")
        else:
            last_message = [objects_in_key[0]]
        self.__download_files_from_keys(
            objects_in_key_list=last_message,
            destination_folder=destination_folder
        )
        message = self.__retrieve_all_messages(
            objects_in_key_list=last_message,
            destination_folder=destination_folder
        )
        self.clear_all_messages(
            path_args=path_args,
            already_retrieved_key=key,
            already_filtered_objects_list=last_message
        )
        return message

    def func_ls(self):
        """
        This function shows the whole AWS Bucket directory structure.
        Return:
            list: A list with all keys that exist inside the whole AWS S3 Bucket.
        """
        summary_list = self.__bucket.summary_all_objects_from_bucket()
        summary_key_list = self.__get_key_values_from_summary(summary_list)
        self.__logger.info("func_ls AWS_S3_Bucket: ")
        for object_key in summary_key_list:
            err_msg = "    " + object_key
            self.__logger.info(err_msg)

        return summary_key_list

    def ls_grep(self, path_args, delimiter_subfolders=False):
        """
        This function filters and shows the AWS Bucket directory structure only for the given
        bucket
        key.
        Args:
            path_args (list): List that contains all path elements in string format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3
            delimiter_subfolders (bool): When this option is enabled it delimits the search
                                         to only the current and designed folder. If it's
                                         disabled it also filter the next subfolders.
        Return:
            list: A list with the retrieved keys that exist inside the given Bucket key/path.
        """
        filter_key = self.__compose_bucket_key(*path_args)
        summary_list = self.__bucket.filter_objects_by_key_on_bucket(
            prefix_filter=filter_key,
            delimiter_subfolders=delimiter_subfolders,
        )
        summary_key_list = self.__get_key_values_from_summary(summary_list)
        err_message = "func_ls grep AWS_S3_Bucket for %s: " % filter_key
        self.__logger.info(err_message)
        for object_key in summary_key_list:
            err_msg = "    " + object_key
            self.__logger.info(err_msg)
        return summary_key_list
