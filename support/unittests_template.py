"""
Unitary tests for your_module.py.

:author: fv4abr0

:contact: antoni.boix@ficosa.com

:since: 2017-06-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from .. import your_module

class TestYourClassName(unittest.TestCase):
    """
    Unitary tests for YourClassName.
    """

    @classmethod
    def setUpClass(cls):
        '''
        Global setUp.
        '''

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        '''
        Test setUp.
        '''

    def tearDown(self):
        '''
        Test tearDown.
        '''

    @classmethod
    def tearDownClass(cls):
        '''
        Global tearDown.
        '''
