# Create Jenkins jobs manually

This is the guide to create the required jenkins jobs for new robotframework libraries. 2 jobs are required: one for continuous integration and one for continuous deployment

## Continuous Integration
In order to get a continuous control over the code quality a CI system is required. The recommended CI tool for T&V projects is [Jenkins](https://jenkins.io/).

The corporative Jenkins installation can be found at http://jenkinsmac.ficosa.com:9090/ but all robotframework/python resources should be placed from this point http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/.

## Jenkins configuration step by step guide
The configuration should be more or less like the configuration of the robotframework-template-ci project. Feel free to use it as template: [robotframework-template Jenkins project](http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/job/robotframework-template-ci/).

### Create the project
- Go to [Jenkins libraries](http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/).
- Log in using your personal credentials.
- Add **New item**.
- Give to the Jenkins project the same **Item name** given to the bitbucket project and adding "-ci" at the end, i.e: robotframework-template-ci.
- Select **Freestyle project**.
- Save by clicking **OK**.

### Configure the project options
- Open the main page of the project, i.e: http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/job/robotframework-template-ci/.
- Open the **configure** page in the left menu of the project.
- Fill the **Description**. Recommendation is use the same description used in the Bitbucket README.md.
- Enable **Restrict where this project can be run** to **s4_slave_2**

  ![jenkins-slave](img/jenkins-restrict-slave.png)

### Configure Source Code Management
- Select **Git**.
- Select the appropiate **Repository URL**, i.e: ssh://git@10.201.235.22:27999/id_lib/robotframework-template.git.
- Select the **developer1 (SSH with Bitbucket)** in the **Credentials** field.

  ![jenkins-vcs](img/jenkins-vcs.jpg)

### Configure Build Triggers
- Enable **Triggers builds remotely** and set the token to **AUTOMATION**. The name of the token can be changed but you would have to consider this modification in future steps.

  ![jenkins-build-triggers](img/jenkins-build-triggers.png)

### Configure Build Environment
- Configure the **Build Environment** as displayed in the image.

  ![jenkins-build-env](img/jenkins-build-env.png)

### Configure Build
The build section defines the sequence of actions executed by the Jenkins slave, in our case the **s4_slave_2**.

- **Add build step** and select **Execute shell**.
- Adapt the following shell script to your project, and use it in the **Execute shell** section. Remember, at least, to change the *demoproject* and *DemoProjectLibrary* references to match your project structure. Also, the source must be changed following the naming conventions but matching your project name.


    #!/bin/bash
    pwd
    source /workspace/JenkinsCi/virtual_environments/libraries/robotframework_aws_bucket/bin/activate
    pip install -U --trusted-host 10.201.236.14 -i http://10.201.236.14:8000/simple pytest pytest-cov mock
    pip install -U --trusted-host 10.201.236.14 -i http://10.201.236.14:8000/simple robotframework
    pip install -U --trusted-host 10.201.236.14 -i http://10.201.236.14:8000/simple -r requirements.txt
    pip freeze
    py.test -v --junitxml nosetests.xml --cov=AWSBucketLibrary --cov=awsbucket --cov=awsbucketwrapper --cov-config .coveragerc --cov-report xml --cov-report term
    pylint -f parseable --rcfile=.pylintrc awsbucket awsbucketwrapper AWSBucketLibrary | tee pylint.out

The final result should be something like that.

![jenkins-build](img/jenkins-build.png)

### Configure Post-build Actions
**Post-build Actions** are used to define actions used to evaluate the output generated in the **Build** section and also generate reports based on these outputs. They are also used to notify third party application about the results of the build. By now the mandatory Post-build Actions are **Publish Cobertura Coverage Report**, **Pubish JUnit test result report**, **Report Violations** and **Notify Stash Instance**. However, you can add more Post-build actions like send emails on failure, etc if you decide these actions are required in your project.

#### Publish Cobertura Coverage Report
- **Add Post-build Action** and select **Publish Cobertura Coverage Report**.
- Configure the Post-build Action based on the quality criteria defined in the **CONTRIBUTING.md**, this is:

  ![jenkins-cobertura](img/jenkins-cobertura.png)

#### Publish JUnit test result report
- **Add Post-build Action** and select **Publish JUnit test result report**.

  ![jenkins-junit](img/jenkins-junit.png)

#### Report Violations
- **Add Post-build Action** and select **Report Violations**.
- Configure the Post-build Action based on the quality criteria defined in the **CONTRIBUTING.md**, this is:

  ![jenkins-violations](img/jenkins-violations.png)

#### Notify Stash Instance
- **Add Post-build Action** and select **Notify Stash Instance**.

  ![jenkins-notify-stash](img/jenkins-notify-stash.png)

## Continuous Deployment
Same as with continuous integration, we need another Jenkins job for the continuous deployment.

## Jenkins configuration step by step guide
The configuration should be more or less like the configuration of the robotframework-template-cd project. Feel free to use it as template: [robotframework-template Jenkins project](http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/job/robotframework-template-cd/).

### Create the project
- Go to [Jenkins libraries](http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/).
- Log in using your personal credentials.
- Add **New item**.
- Give to the Jenkins project the same **Item name** given to the bitbucket project and adding "-cd" at the end, i.e: robotframework-template-cd.
- Select **Freestyle project**.
- Save by clicking **OK**.

*Pro tip:* You could copy existing item from the "robotframework-template-ci" and configure it to match the specified configuration below. 

### Configure the project options
- Open the main page of the project, i.e: http://jenkinsmac.ficosa.com:9090/job/T+V/job/Libraries/job/robotframework-template-cd/.
- Open the **configure** page in the left menu of the project.
- Fill the **Description**. Just as continuous deployment for robotframework-template.
- Enable **Restrict where this project can be run** to **s4_slave_2**

  ![jenkins-slave](img/jenkins-restrict-slave.png)

### Configure Source Code Management
- Select **Git**.
- Select the appropiate **Repository URL**, i.e: ssh://git@10.201.235.22:27999/id_lib/robotframework-template.git.
- Select the **developer1 (SSH with Bitbucket)** in the **Credentials** field.

The same as ci job.

  ![jenkins-vcs](img/jenkins-vcs.jpg)

### Configure Build Triggers
- Enable **Triggers builds remotely** and set the token to **AUTOMATION**. The name of the token can be changed but you would have to consider this modification in future steps.

  ![jenkins-build-triggers](img/jenkins-build-triggers.png)

### Configure Build Environment
- Configure the **Build Environment** as displayed in the image.

The same as ci job.

  ![jenkins-build-env](img/jenkins-build-env.png)

### Configure Build
The build section defines the sequence of actions executed by the Jenkins slave, in our case the **s4_slave_2**.

- **Add build step** and select **Execute shell**.
- Adapt the following shell script to your project, and use it in the **Execute shell** section. The only part to adapt is the source line to match your project.


    #!/bin/bash
    pwd
    source /workspace/JenkinsCi/virtual_environments/libraries/robotframework_template/bin/activate
    echo "Starting deployment..."
    python setup.py sdist
    python libdoc_helper.py
    cp dist/* /home/sqaidneo/pypiserver/packages/
    cp docs/* /home/sqaidneo/workspace/robotframework-libraries-docs/

The final result should be something like that.

![jenkins-build](img/jenkins-build-cd.png)

This job doesn't need Post-build actions, so if you copied the ci job delete all of them from the cd job.