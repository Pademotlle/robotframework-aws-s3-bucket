# support/
This folder should be used to contain any support file (neither project related documentation nor source code) used during the develpment process. The files stored in this folder `should not be distributed`.

Typical examples of files contained here can be, code templates, configuration file templates, bug logs used in the development process, etc. 
