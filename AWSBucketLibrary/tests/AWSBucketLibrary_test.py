"""
Unitary tests for AWSBucketLibrary module.

:author: Xavi

:date: 2018-10-16
"""

# pylint:disable=C0103,C0111

import unittest
import mock

from AWSBucketLibrary import AWSBucketLibrary


class TestAWSBucketLibrary(unittest.TestCase):
    """
    Unitary testing for AWSBucketLibrary class.
    """

    __HTTP_PROXY_IP_ADDRESS = u"100.20.30.40:0123"

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        cls.valid_bucket_name = u"Bucket1"
        cls.valid_certificate_id = u"[certid_1]"
        cls.valid_bucket_folder_name = u"folder_1"
        cls.valid_object_key = u"[certid_1]/folder_1"
        cls.valid_imei = u"0123456789876543210"
        cls.valid_dest_folder = u"C:/Temp"
        cls.valid_proxy = cls.__HTTP_PROXY_IP_ADDRESS
        cls.valid_boolean_option = False

        cls.invalid_bucket_name = "Bucket1"
        cls.invalid_certificate_id = 123456789
        cls.invalid_object_key = list(cls.valid_object_key)
        cls.invalid_dest_folder = list(cls.valid_dest_folder)
        cls.invalid_imei = 123456789876543210
        cls.invalid_proxy = 123.456
        cls.invalid_boolean_option = "False"

    def setUp(self):
        """
        Test setUp.
        """
        self.path_args = []
        self.invalid_path_arg = {}
        self.bucket_robot = AWSBucketLibrary()

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_connect_to_bucket(self):
        """
        Test for the keyword that uses the connect_to_bucket function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.connect_to_bucket = mock.Mock()
        self.bucket_robot.connect_to_bucket(
            bucket_name=self.valid_bucket_name,
            access_key=self.valid_object_key,
            secret_key=self.valid_object_key,
        )
        self.bucket_robot._AWSBucketLibrary__aws_bucket.connect_to_bucket.\
            assert_called_once_with(
                bucket_name=str(self.valid_bucket_name),
                access_key=str(self.valid_object_key),
                secret_key=str(self.valid_object_key),
                http_proxy="",
                https_proxy=""
            )

    def test_connect_to_bucket_invalid_argument(self):
        """
        Test for the keyword that uses the connect_to_bucket function with only one of
        the input arguments in an invalid format, so a TypeError exception shall be raised
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.connect_to_bucket = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.connect_to_bucket(
                bucket_name=self.valid_bucket_name,
                access_key=self.valid_object_key,
                secret_key=self.valid_object_key,
                http_proxy=self.valid_proxy,
                https_proxy=self.invalid_proxy,
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_connect_to_bucket_all_arguments_invalid(self):
        """
        Test for the keyword that uses the connect_to_bucket function with all input
        arguments in an invalid format, so a TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.connect_to_bucket = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.connect_to_bucket(
                bucket_name=self.invalid_bucket_name,
                access_key=self.invalid_object_key,
                secret_key=self.invalid_object_key,
                http_proxy=self.invalid_proxy,
                https_proxy=self.invalid_proxy,
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_clean_all_messages(self):
        """
        Test for the keyword that uses the clean_all_messages function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.clear_all_messages = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        self.bucket_robot.clean_all_messages(
            self.path_args
        )
        self.bucket_robot._AWSBucketLibrary__aws_bucket.clear_all_messages.\
            assert_called_once_with(path_args=self.path_args)

    def test_clean_all_messages_invalid_argument(self):
        """
        Test for the keyword that uses the clean_all_messages function with only one of
        the input arguments in an invalid format, so a TypeError exception shall be raised
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.clear_all_messages = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.invalid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.clean_all_messages(
                self.path_args
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_clean_all_messages_all_arguments_invalid(self):
        """
        Test for the keyword that uses the clean_all_messages function with all input
        arguments in an invalid format, so a TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.clear_all_messages = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.clean_all_messages(
                self.path_args
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_clear_all_messages_path_argument_invalid_format(self):
        """
        Test for the keyword that uses the ls_grep function with the input path_args
        argument in invalid format.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.clean_all_messages = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.clean_all_messages(
                path_args=self.invalid_path_arg,
            )
        self.assertEqual(
            str(err.exception),
            "Please enter a valid list format that contains all path "
            "elements inside."
        )

    def test_get_incoming_messages(self):
        """
        Test for the keyword that uses the get_incoming_messages function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_incoming_messages = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        self.bucket_robot.get_incoming_messages(
            path_args=self.path_args,
            destination_folder=self.valid_dest_folder
        )
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_incoming_messages.\
            assert_called_once_with(
                path_args=self.path_args,
                destination_folder=self.valid_dest_folder
            )

    def test_get_incoming_messages_invalid_argument(self):
        """
        Test for the keyword that uses the get_incoming_messages function with only one of
        the input arguments in an invalid format, so a TypeError exception shall be raised
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_incoming_messages = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_incoming_messages(
                path_args=self.path_args,
                destination_folder=self.invalid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_get_incoming_messages_all_arguments_invalid(self):
        """
        Test for the keyword that uses the get_incoming_messages function with all input
        arguments in an invalid format, so a TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_incoming_messages = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_incoming_messages(
                path_args=self.path_args,
                destination_folder=self.invalid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_get_incoming_messages_path_argument_invalid_format(self):
        """
        Test for the keyword that uses the ls_grep function with the input path_args
        argument in invalid format.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_incoming_messages = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_incoming_messages(
                path_args=self.invalid_path_arg,
                destination_folder=self.valid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "Please enter a valid list format that contains all path "
            "elements inside."
        )

    def test_get_oldest_message(self):
        """
        Test for the keyword that uses the get_oldest_message function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_oldest_message = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        self.bucket_robot.get_oldest_message(
            path_args=self.path_args,
            destination_folder=self.valid_dest_folder
        )
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_oldest_message.\
            assert_called_once_with(
                path_args=self.path_args,
                destination_folder=self.valid_dest_folder
            )

    def test_get_oldest_message_invalid_argument(self):
        """
        Test for the keyword that uses the get_oldest_message function with only one of
        the input arguments in an invalid format, so a TypeError exception shall be raised
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_oldest_message = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_oldest_message(
                path_args=self.path_args,
                destination_folder=self.invalid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_get_oldest_message_all_arguments_invalid(self):
        """
        Test for the keyword that uses the get_incoming_messages function with all input
        arguments in an invalid format, so a TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_oldest_message = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_oldest_message(
                path_args=self.path_args,
                destination_folder=self.invalid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "All input arguments shall be entered in string format. "
            "Please check all entered input arguments format."
        )

    def test_get_oldest_message_path_argument_invalid_format(self):
        """
        Test for the keyword that uses the ls_grep function with the input path_args
        argument in invalid format.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.get_oldest_message = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.get_oldest_message(
                path_args=self.invalid_path_arg,
                destination_folder=self.valid_dest_folder
            )
        self.assertEqual(
            str(err.exception),
            "Please enter a valid list format that contains all path "
            "elements inside."
        )

    def test_ls(self):
        """
        Test for the keyword that uses the func_ls function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.func_ls = mock.Mock()
        self.bucket_robot.ls()
        self.bucket_robot._AWSBucketLibrary__aws_bucket.func_ls.assert_called_once()

    def test_ls_grep(self):
        """
        Test for the keyword that uses the ls_grep function
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        self.bucket_robot.ls_grep(
            path_args=self.path_args,
            delimiter_subfolders=self.valid_boolean_option
        )
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep.\
            assert_called_once_with(
                path_args=self.path_args,
                delimiter_subfolders=self.valid_boolean_option
            )

    def test_ls_grep_invalid_argument(self):
        """
        Test for the keyword that uses the ls_grep function with only one of
        the input arguments in an invalid format, so a TypeError exception shall be raised
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.ls_grep(
                path_args=self.path_args,
                delimiter_subfolders=self.valid_boolean_option
            )
        self.assertEqual(
            str(err.exception),
            "All path arguments from the input list shall be entered in string format."
        )

    def test_ls_grep_all_arguments_invalid(self):
        """
        Test for the keyword that uses the ls_grep function with all input
        arguments in an invalid format, so a TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep = mock.Mock()

        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_certificate_id)
        self.path_args.append(self.invalid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.ls_grep(
                path_args=self.path_args,
                delimiter_subfolders=self.valid_boolean_option
            )
        self.assertEqual(
            str(err.exception),
            "All path arguments from the input list shall be entered in string format."
        )

    def test_ls_grep_path_argument_invalid_format(self):
        """
        Test for the keyword that uses the ls_grep function with the input path_args
        argument in invalid format.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep = mock.Mock()

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.ls_grep(
                path_args=self.invalid_path_arg,
                delimiter_subfolders=self.valid_boolean_option
            )
        self.assertEqual(
            str(err.exception),
            "Please enter a valid list format that contains all path "
            "elements inside."
        )

    def test_ls_grep_delimiter_option_invalid(self):
        """
        Test for the keyword that uses the ls_grep function with delimiter_subfolders
        argument in an invalid format, so a different TypeError exception shall be raised.
        """
        self.bucket_robot._AWSBucketLibrary__aws_bucket.ls_grep = mock.Mock()

        self.path_args.append(self.valid_certificate_id)
        self.path_args.append(self.valid_bucket_folder_name)
        self.path_args.append(self.valid_imei)

        with self.assertRaises(TypeError) as err:
            self.bucket_robot.ls_grep(
                path_args=self.path_args,
                delimiter_subfolders=self.invalid_boolean_option
            )
        self.assertEqual(
            str(err.exception),
            "Delimiter_subfolder input argument shall be entered in a boolean format."
        )

