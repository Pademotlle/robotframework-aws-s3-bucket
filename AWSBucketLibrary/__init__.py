"""
RIDE generic AWS Bucket communication interface.

:author: Xavi
:since: 2018-10-16
"""

# pylint:disable=R0913

import logging
from robot.api.deco import keyword
from awsbucketwrapper import aws_bucket_wrapper


class AWSBucketLibrary(object):
    """
    Robotframework client interface that interacts with an Amazon Web Service S3 Bucket
    and its all incoming messages sent from a TCU.
    """
    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        self.__aws_bucket = aws_bucket_wrapper.AWSBucketWrapper()
        self.logger = logging.getLogger(__name__)

    @keyword("Connect to Bucket")
    def connect_to_bucket(self, bucket_name, access_key, secret_key, http_proxy="",
                          https_proxy=""):
        """
        This function establishes a connection to the desired AWS S3 Bucket.

        *Args:*

            ``bucket_name`` (str): AWS S3 Bucket name

            ``access_key`` (str): Provided key that grants access to the AWS S3 Bucket.

            ``secret_key`` (str): Provided key that grants access to the AWS S3 Bucket.

            ``http_proxy`` (str): IP address for a possible proxy server. i.e: "10.23.45.67:8901"

            ``https_proxy`` (str): IP address for a possible proxy server. i.e: "10.23.45.67:8901"
        """
        args_list = [bucket_name, access_key, secret_key, http_proxy, https_proxy]
        if not all(isinstance(input_arg, (str, unicode)) for input_arg in args_list):
            raise TypeError("All input arguments shall be entered in string format. "
                            "Please check all entered input arguments format.")
        self.__aws_bucket.connect_to_bucket(
            bucket_name=str(bucket_name),
            access_key=str(access_key),
            secret_key=str(secret_key),
            http_proxy=str(http_proxy),
            https_proxy=str(https_proxy)
        )

    @keyword("Clean all messages")
    def clean_all_messages(self, path_args):
        """
        This function "clears" all received messages by moving all existing objects inside
        the given Bucket directory (key) into the folder "all_messages", that also exists inside
        the same given Bucket directory (so the messages are not fully removed).
                i.e.: key/file_name --> key/all_messages/file_name

        *Args:*

            ``path_args`` (list): List that contains all path elements in string format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3
        """
        if not isinstance(path_args, list):
            raise TypeError("Please enter a valid list format that contains all path "
                            "elements inside.")
        if not all(isinstance(input_arg, (str, unicode)) for input_arg in path_args):
            raise TypeError("All input arguments shall be entered in string format. "
                            "Please check all entered input arguments format.")
        self.__aws_bucket.clear_all_messages(path_args=path_args)

    @keyword("Get incoming messages")
    def get_incoming_messages(self, path_args, destination_folder):
        """
        This function retrieves last messages received in the AWS S3 Bucket source at a concrete
        directory (key) for a concrete TCU.

        *Args:*

            ``path_args`` (list): List that contains all Bucket path (key) elements in string
            format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3

            ``destination_folder`` (str): Absolute host path of the folder where will be placed all
                                      downloaded message files.

        *Return:*

            ``list``: List with all retrieved messages in a dictionary format.
        """
        if not isinstance(path_args, list):
            raise TypeError("Please enter a valid list format that contains all path "
                            "elements inside.")
        if not all(isinstance(input_arg, (str, unicode)) for input_arg in path_args):
            raise TypeError("All input arguments shall be entered in string format. "
                            "Please check all entered input arguments format.")
        return self.__aws_bucket.get_incoming_messages(
            destination_folder=str(destination_folder),
            path_args=path_args
        )

    @keyword("Get oldest message from server")
    def get_oldest_message(self, path_args, destination_folder):
        """
        This function retrieves the oldest message received in the AWS S3 Bucket source at a
        concrete directory (key) for a concrete TCU.

        *Args:*

            ``path_args`` (list): List that contains all Bucket path (key) elements in string
            format.
                              i.e.: ["element1", "element2", "element3"] -->
                              element1/element2/element3

            ``destination_folder`` (str): Absolute host path of the folder where will be placed all
                                      downloaded message files.

        *Return:*

            ``message``: oldest message in a dictionary format.
        """
        if not isinstance(path_args, list):
            raise TypeError("Please enter a valid list format that contains all path "
                            "elements inside.")
        if not all(isinstance(input_arg, (str, unicode)) for input_arg in path_args):
            raise TypeError("All input arguments shall be entered in string format. "
                            "Please check all entered input arguments format.")
        return self.__aws_bucket.get_oldest_message(
            destination_folder=str(destination_folder),
            path_args=path_args
        )

    @keyword("func_ls")
    def ls(self):
        """
        This function shows and returns a list of the whole AWS Bucket directory structure.

        *Return:*

            ``list``: A list with all keys that exist inside the whole AWS S3 Bucket.
        """
        return self.__aws_bucket.func_ls()

    @keyword("func_ls grep")
    def ls_grep(self, path_args, delimiter_subfolders=False):
        """
        This function shows the AWS Bucket directory structure only for the given bucket key.

        *Args:*

            ``path_args`` (list): List that contains all Bucket path (key) elements in string
            format.

            ``delimiter_subfolders`` (bool): When this option is enabled it delimits the search
                                         to only the current and designed folder. If it's
                                         disabled it also filter the next subfolders.

        *Return:*

            ``list``: A list with the retrieved keys that exist inside the given Bucket key/path.
        """
        if not isinstance(path_args, list):
            raise TypeError("Please enter a valid list format that contains all path "
                            "elements inside.")
        if not all(isinstance(input_arg, (str, unicode)) for input_arg in path_args):
            raise TypeError(
                "All path arguments from the input list shall be entered in string format.")
        if not isinstance(delimiter_subfolders, bool):
            raise TypeError("Delimiter_subfolder input argument shall be entered in a "
                            "boolean format.")
        return self.__aws_bucket.ls_grep(
            path_args=path_args,
            delimiter_subfolders=delimiter_subfolders
        )
