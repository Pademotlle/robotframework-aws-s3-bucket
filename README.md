# Robotframework AWS Bucket library 

This library python project contains a robotframework adapted python code that allows the connection and the use of AWS
Bucket services and features.

Basically the AWS S3 Bucket features exposed are:
- Connect to the desired AWS S3 Bucket in order to interact with him
- Get all messages of a concrete path (key) inside the AWS S3 Bucket directory (and after that clean them from the directory).
- Clean all messages of a concrete path (key) inside the AWS S3 Bucket directory. This feature is not removing any message, it's just moving and placing all messages to a folder in the same path (key) named 'all_messages'.
- Get an overview of the whole AWS S3 Bucket directory structure.
- Get an overview of a concrete part (depending of the given path/key) in the AWS S3 Bucket directory structure. 
