*** Settings ***
Library           AWSBucketLibrary
Library           Collections

*** Variables ***
${bucket_name}    bucket_name
${access}         access_key
${secret}         secret
${certificate_id}    cert_id
${folder}         folder
${value}           value
${destination_folder}    C:/Temp

*** Test Cases ***
Check all directory structure
    [Documentation]    First of all is necessary to connect to the desired bucket. In order to do it the function AWSBucketLibrary.Connect_to_Bucket stablishes a connection with it.
    ...    The access_key and the secret_key shall be provided by the AWS Bucket admin.
    ...
    ...    This test case is gettin' all possible object keys from the AWS Bucket (a key is the whole path of a directory inside the AWS Bucket) and returning a list with all of them in unicode format.
    AWSBucketLibrary.Connect To Bucket    bucket_name=${bucket_name}    access_key=${access}    secret_key=${secret}
    ${ls_list}    AWSBucketLibrary.Ls
    Collections.Log List    ${ls_list}

Clean all files from path
    [Documentation]    First of all is necessary to connect to the desired bucket. In order to do it the function 'AWSBucketLibrary.Connect_to_Bucket' stablishes a connection with it.
    ...    The access_key and the secret_key shall be provided by the AWS Bucket admin.
    ...
    ...    This test case is moving all messages that exist inside the given AWS Bucket key into a folder called 'all_messages' that is placed in the same key directory.
    ...    The point to do that is to don't delete any message from the Bucket.
    ...
    ...    **A key is the whole path of a directory inside the AWS Bucket.**
    AWSBucketLibrary.Connect To Bucket    bucket_name=${bucket_name}    access_key=${access}    secret_key=${secret}
    AWSBucketLibrary.Clean All Messages    ${certificate_id}    ${folder}    ${imei}

Download all files from path
    [Documentation]    First of all is necessary to connect to the desired bucket. In order to do it the function 'AWSBucketLibrary.Connect_to_Bucket' stablishes a connection with it.
    ...    The access_key and the secret_key shall be provided by the AWS Bucket admin.
    ...
    ...    This test case is retrieving all messages that exists inside the given key and downloads them into the given hosting path (Windows or Linux path). The function 'AWSBucketLibrary.Get_Incoming_Messages' returns the information from the JSON messages files in list of Python dictionaries.
    ...
    ...    After performing the download and the retribution of all messages information then all these message files are moved to the 'all_messages' folder.
    ...
    ...    **A key is the whole path of a directory inside the AWS Bucket.**
    AWSBucketLibrary.Connect To Bucket    bucket_name=${bucket_name}    access_key=${access}    secret_key=${secret}
    ${msg_list}    AWSBucketLibrary.Get Incoming Messages    ${certificate_id}    ${folder}    ${imei}    ${destination_folder}
    Collections.Log List    ${msg_list}
