"""
Auto libdoc generator script. This script looks for robotframework libraries and generates a
documentation HTML file for each one using robot.libdoc functionalities. No configuration is
required if the Robotframework libraries have been created following the naming conventions
defined in the CONTRIBUTING.md file.

:author: Xavi
:since: 2018-16-10
"""

import subprocess
import re
import os
import argparse

import setuptools
import awsbucket


LIBDOC_CMD = "python -m robot.libdoc -v {version} {source} docs/{output}"

ROBOT_LIBRARIES_PATTERN = re.compile(r'^([A-Z]{1}.+)+Library$')
ROBOT_LIBRARIES_FILES = re.compile(r'^(?P<name>(([A-Z]{1}.+)+)|(__init__))\.py$')


def _robot_lib_in_package(package):
    lib_list = []
    matching_list = [
        ROBOT_LIBRARIES_FILES.match(lib) for lib
        in os.listdir(package.replace(".", "/"))
        if bool(ROBOT_LIBRARIES_FILES.match(lib))
    ]

    for libf in matching_list:
        if libf.groupdict().get('name') == "__init__":
            lib_list.append(package)
        else:
            lib_list.append(package + "." + libf.groupdict().get('name'))

    return lib_list


def _main(_):
    """
    Main functionality.
    """

    lib_pkg = [pkg for pkg in setuptools.find_packages() if ROBOT_LIBRARIES_PATTERN.match(pkg)]

    lib_list = []
    for pkg in lib_pkg:
        lib_list += _robot_lib_in_package(pkg)

    print 'Robotframework libraries found:'
    for libf in lib_list:
        print libf
    print 'Auto generating HTML based documentation ...'

    for libf in lib_list:
        process = subprocess.Popen(
            LIBDOC_CMD.format(
                version=awsbucket.__version__, source=libf, output=libf + '.html'),
            shell=True
        )
        process.communicate()


def _parse_args():
    parser = argparse.ArgumentParser(description='Libdoc generator')
    return parser.parse_args()


if __name__ == "__main__":
    _main(_parse_args())
