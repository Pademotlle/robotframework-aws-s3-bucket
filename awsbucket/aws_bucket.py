"""
Interface that creates a connection to a concrete AWS S3 Bucket.

:author: Xavi
:date: 2018-10-19
"""

import logging

from awsbucket import aws_client_session


class AWSBucketBaseError(Exception):
    """
    Base module error.
    """


class AWSBucketConnectionError(AWSBucketBaseError):
    """
    AWS Bucket connection error.
    """


class AWSBucketNotConnectedError(AWSBucketBaseError):
    """
    No connection established with AWS Bucket.
    """


class AWSCopyToBucketError(AWSBucketBaseError):
    """
    Copying object from origin Bucket to source Bucket failed.
    """


class AWSDeleteObjectsFromBucketError(AWSBucketBaseError):
    """
    Copying object from origin Bucket to source Bucket failed.
    """


class AWSDownloadObjectFromBucketError(AWSBucketBaseError):
    """
    Downloading object from origin Bucket to source Bucket failed.
    """


class AWSUploadObjectFromBucketError(AWSBucketBaseError):
    """
    Uploading object from origin Bucket to source Bucket failed.
    """


class AWSBucket(object):
    """
    Main class used to connect and interact with a Amazon Web Service S3 Bucket.
    """

    def __init__(self):
        self.__logger = logging.getLogger(__name__)
        self.__session_exists = False
        self.__bucket_session = None
        self.__client_session = None
        self.__bucket = None
        self.__bucket_name = ""
        self.__connection_already_established = False

    def __check_existing_session(self):
        """
        Checks if there is any already existing session. If not, it creates a new session.
        """
        if not self.__session_exists:
            self.__create_session()
            self.__session_exists = True

    def __check_bucket_connection(self):
        """
        Checks if there is any already existing session. If not, creates a new session.
        """
        if not self.__bucket:
            raise AWSBucketNotConnectedError(
                "No connection established with any AWS Bucket. "
                "Please, connect to a AWS Bucket previously.")

    def __create_session(self):
        """
        Creates a new session resource in order to establish a connection with an AWS Service.
        """
        self.__client_session = aws_client_session.AWSClient()
        self.__bucket_session = self.__client_session.session

    def set_session_keys(self, access_key, secret_key):
        """
        Updates session resource proxy settings configuration.
        """
        self.__check_existing_session()
        self.__client_session.update_session_keys(
            aws_access_key=access_key,
            aws_secret_key=secret_key
        )
        self.__bucket_session = self.__client_session.session

    def configure_proxy_settings(self, http_proxy="", https_proxy=""):
        """
        Updates session resource proxy settings configuration.
        """
        self.__check_existing_session()
        self.__client_session.update_proxy_configuration(
            http_proxy=http_proxy,
            https_proxy=https_proxy)
        self.__bucket_session = self.__client_session.session
        if self.__connection_already_established:
            self.connect_to_bucket(bucket_name=self.__bucket_name)

    def connect_to_bucket(self, bucket_name):
        """
        Creates and establishes a new connection to the desired Bucket.
        """
        self.__bucket_name = bucket_name
        self.__check_existing_session()
        self.__logger.info(
            "Connecting to Bucket: '{bucket_name}'".format(
                bucket_name=bucket_name))
        try:
            self.__bucket = self.__bucket_session.Bucket(bucket_name)
            self.__connection_already_established = True
            self.__logger.info("Successfully connected to the AWS Bucket.")

        except Exception:
            self.__logger.exception("Unexpected Exception: ")
            raise AWSBucketConnectionError(
                "Could establish connection with the '{bucket_name}' AWS Bucket.".format(
                    bucket_name=bucket_name))

    def copy_file_from_source(self,
                              origin_bucket_name="",
                              origin_key_object="",
                              destination_key=""):
        """
        Copy an object from one S3 location to an object in this bucket.
        Args:
            origin_bucket_name (str): The name of the source Bucket from the file
                                      is being to copied to.
            origin_key_object (str): The path/key name of the source object.
            destination_key (str): The name of the path/key to copy to.
        """
        self.__check_bucket_connection()
        try:
            self.__logger.info(
                "Copying file from '{origin_key}' origin AWS Bucket to target "
                "AWS Bucket: '{target_key}'".format(
                    origin_key=origin_key_object,
                    target_key=destination_key))
            self.__bucket.copy(
                CopySource={
                    "Bucket": origin_bucket_name,
                    "Key": origin_key_object
                },
                Key=destination_key)
            self.__logger.info(
                "File has been copied successfully to AWS Bucket: "
                "'{target_key}'".format(
                    target_key=destination_key))
        except Exception:
            self.__logger.exception('Unexpected Exception: ')
            raise AWSCopyToBucketError(
                "Something went wrong copying file to bucket destination."
                "Please, check origin and destination paths.")

    def delete_file_from_bucket(self, delete):
        """
        This operation enables you to delete multiple objects from a bucket using a single HTTP request.
        You may specify up to 1000 keys.
        Args:
            delete (dict)= It shall contain 'Objects' list with the key names of the objects
                           to be deleted.
                    i.e.: Delete = {'Objects':
                                        [
                                            {
                                                'Key': 'string',
                                                'VersionId': 'string'
                                            }
                                        ],

        Returns:
            dict = A dictionary with the deleted objects list and the errors.
                   i.e.: {
                            'Deleted':
                            [
                                {
                                    'Key': 'string',
                                    'VersionId' : 'string',
                                    'DeleteMarker': True/False,
                                    'DeleteMarkerVersionId': 'string'
                                },
                            ],
                            'Errors': [
                                {
                                    'Key': 'string',
                                    'VersionId' : 'string',
                                    'Code': 'string',
                                    'Message': 'string'
                                },
                            ]
                        }

        """
        self.__check_bucket_connection()
        try:
            self.__logger.info("Deleting following objects from AWS S3 Bucket:")
            for obj_to_delete in delete['Objects']:
                self.__logger.info("    Key: '{key_deleted}'".format(
                    key_deleted=obj_to_delete['Key']))

            deletion_response = self.__bucket.delete_objects(Delete=delete)
            self.__logger.info("Deletion has been successful.")
        except Exception:
            self.__logger.exception("Unexpected Exception: ")
            raise AWSDeleteObjectsFromBucketError(
                "Something went wrong deleting file on bucket destination."
                "Please, check deletion input dictionary file.")
        return deletion_response

    def move_file_from_bucket(
            self, origin_bucket_name="", origin_key_object="", destination_key=""):
        """
        Move an object from one S3 location to an object in this bucket. After that, the object
        is deleted.
        Args:
            origin_bucket_name (str): The name of the source Bucket from the file
                                      is being to copied to.
            origin_key_object (str): The path/key name of the source object.
            destination_key (str): The name of the path/key to copy to.
        """
        self.__check_bucket_connection()
        self.copy_file_from_source(
            origin_bucket_name=origin_bucket_name,
            origin_key_object=origin_key_object,
            destination_key=destination_key
        )
        delete_dict = {
            'Objects':
                [
                    {
                        'Key': origin_key_object,
                    }
                ]
        }
        return self.delete_file_from_bucket(delete=delete_dict)

    def download_file_from_bucket(self, file_key, destination_path):
        """
        This function downloads and retrieves a file from AWS Bucket source to a target
        location.
        Args:
            file_key (str): Target AWS S3 Bucket key + file name to download.
            destination_path (str): Target directory where the downloaded file will be
                                    placed.
        """
        if not isinstance(file_key, str) or not isinstance(destination_path, str):
            raise TypeError("Both bucket object target key or destination path shall be in"
                            "string format.")

        self.__check_bucket_connection()

        try:
            self.__logger.info(
                "Downloading file from AWS S3 Bucket in: '{file_key}', on path: '{dest_path}'".format(
                    file_key=file_key,
                    dest_path=destination_path))

            self.__bucket.download_file(
                Key=file_key,
                Filename=destination_path)

            self.__logger.info("Download process has finished.")
        except Exception:
            self.__logger.exception('Unexpected Exception: ')
            raise AWSDownloadObjectFromBucketError(
                "Something went wrong downloading file from AWS S3 Bucket.")

    def upload_file_to_bucket(self, file_to_upload, target_key):
        """
        This function uploads a file to an AWS Bucket concrete key.
        Args:
            file_to_upload (str): Path and name for the desired file to upload.
            target_key (str): AWS S3 Bucket key name with the target location where the
                              uploaded file will be placed.
        """
        if not isinstance(file_to_upload, str) or not isinstance(target_key, str):
            raise TypeError(r"Both object path/key to upload or target path inside Bucket "
                            "shall be in string format.")

        self.__check_bucket_connection()

        try:
            self.__logger.info(
                "Uploading file to AWS S3 Bucket on path: '{target_key}'".format(
                    target_key=target_key))

            self.__bucket.upload_file(
                Filename=file_to_upload,
                Key=target_key)

            self.__logger.info("Upload process has finished.")
        except Exception:
            self.__logger.exception('Unexpected Exception: ')
            raise AWSUploadObjectFromBucketError(
                "Something went wrong uploading file to AWS S3 Bucket.")

    def summary_all_objects_from_bucket(self):
        """
        Retrieves a summary of all objects that are located in AWS Bucket in a list format.
        Returns:
            list: A list that contains all keys (in str format) that exists in the AWS Bucket.
        """
        self.__check_bucket_connection()
        self.__logger.info("Getting all objects existing in the AWS S3 Bucket.")
        try:
            return list(self.__bucket.objects.all())
        except Exception:
            self.__logger.exception('Unexpected Exception: ')
            raise AWSBucketConnectionError(
                "Something went wrong during the process. Please, retry this "
                "process again or check the Bucket's connection.")

    def filter_objects_by_key_on_bucket(self, prefix_filter, delimiter_subfolders=False):
        """
        Looks for objects locations by filtering them by they key/path route prefix.
        Returns a list with all key routes that matches with the given prefix key.
        Args:
            prefix_filter (str):  The key/path that shall match with the desired object key/path
                                  in the Bucket. As it's a prefix, the key/route shall be specified
                                  from the beginning of the key to the desired part of the path
                                  (from left to right).
            delimiter_subfolders (bool): If enabled it delimiters the filter setting in order
                                         to avoid the subfolders inside the resulting key.
        Returns:
            list: A list that contains all keys (in str format) that matches with the given prefix.
        """
        if not isinstance(prefix_filter, str):
            raise TypeError("The used prefix filter entered is not an string. "
                            "Please, check the input format.")
        self.__check_bucket_connection()
        self.__logger.info(
            "Getting all objects existing in the AWS S3 Bucket that their "
            "key starts with: '{prefix_filter}'".format(
                prefix_filter=prefix_filter))
        try:
            if delimiter_subfolders:
                filtered = self.__bucket.objects.filter(
                    Prefix=prefix_filter,
                    Delimiter="/")
            else:
                filtered = self.__bucket.objects.filter(
                    Prefix=prefix_filter)
            self.__logger.info("Filtered objects: ")
            filtered_list = list(filtered)
            for key in filtered_list:
                self.__logger.info(key)
            return filtered_list
        except Exception:
            self.__logger.exception('Unexpected Exception: ')
            raise AWSBucketConnectionError(
                "Something went wrong during the process. Please, retry this "
                "process again, check the input filter_prefix or check the "
                "Bucket's connection.")
