"""
Session client that allows a connection to the AWS Bucket.

:author: Xavi
:date: 2018-10-16

"""

import logging
import boto3

from botocore import config
from requests.packages import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.InsecurePlatformWarning)


class AWSClientBaseError(Exception):
    """
    Base module error.
    """


class AWSCreationSessionError(AWSClientBaseError):
    """
    AWS Session creation error
    """


class AWSResourceConfigurationError(AWSClientBaseError):
    """
    AWS Resource creation error
    """


class AWSClient(object):
    """
    Main class used to create a session that allows to communicate with the AWS Bucket.
    """

    _S3_RESOURCE_NAME = "s3"

    def __init__(self, aws_access_key="", aws_secret_key=""):
        """
        AWSClient Constructor
        """
        self.__client_logger = logging.getLogger(__name__)
        self.__client_session = None
        self.__client_session_resource = None
        self.__aws_access_key = aws_access_key
        self.__aws_secret_key = aws_secret_key
        self.__http_proxy = ""
        self.__https_proxy = ""

    @property
    def session(self):
        return self.__client_session_resource

    def create_client_session(self):
        """
        Create a new client session connected to Amazon Web Service.
        """
        self.__client_logger.info("Creating client session...")
        try:
            self.__client_session = boto3.Session(
                aws_access_key_id=self.__aws_access_key,
                aws_secret_access_key=self.__aws_secret_key)
            self.__client_logger.info("Client session has been created.")
            self.__update_resource_configuration()
            return self.__client_session_resource

        except Exception:
            self.__client_logger.exception("Unexpected Exception: ")
            raise AWSCreationSessionError("Could not create a session to AWS service.")

    def update_session_keys(self, aws_access_key="", aws_secret_key=""):
        """
        Updates a new client session connected to Amazon Web Service with different a access key and secret key.
        (It's needed to create a new session a after that a new session resource).

        Args:
            aws_access_key (str): Access key needed to create a session that connects with an AWS Bucket.
            aws_secret_key (str): Secret key needed to create a session that connects with an AWS Bucket.
        """
        if not isinstance(aws_access_key, str) or not isinstance(aws_secret_key, str):
            raise TypeError("Session keys should be strings!")
        self.__aws_access_key = aws_access_key
        self.__aws_secret_key = aws_secret_key
        self.create_client_session()

    def update_proxy_configuration(self, http_proxy="", https_proxy=""):
        """
        Updates the current session resource with a new proxy configuration.
        (It's needed to create a new session resource).
        Args:
            http_proxy (str): HTTP proxy server IP address (i.e.: 10.23.45.67:8910)
            https_proxy (str): HTTPS proxy server IP address (i.e.: 10.23.45.67:8910)
        """
        if not isinstance(http_proxy, str) or not isinstance(https_proxy, str):
            raise TypeError("Session proxy addresses should be strings!")
        self.__http_proxy = http_proxy
        self.__https_proxy = https_proxy
        self.__update_resource_configuration()

    def __create_session_resource(self):
        """
        Re-creates a new session resource without proxy configuration.
        """
        self.__client_session_resource = self.__client_session.resource(
            service_name=self._S3_RESOURCE_NAME,
            verify=False,)

    def __update_resource_configuration(self):
        """
        Re-creates a new session resource with or without proxy configuration.
        """
        self.__client_logger.info("Updating session resource configuration...")
        try:
            if self.__http_proxy or self.__https_proxy:
                self.__client_session_resource = self.__client_session.resource(
                    service_name=self._S3_RESOURCE_NAME,
                    verify=False,
                    config=config.Config(
                        proxies={"http": self.__http_proxy,
                                 "https": self.__https_proxy}))
            else:
                self.__create_session_resource()
            self.__client_logger.info("Session resource configuration has been updated.")
        except Exception:
            self.__client_logger.exception("Unexpected Exception: ")
            raise AWSResourceConfigurationError("Could not update properly the session "
                                                "resource configuration.")
