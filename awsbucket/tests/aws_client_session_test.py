"""
Unitary testing for AWS Client Session module.

:author: Xavi


:date: 2018-10-17
"""

import logging
import mock
import unittest

from awsbucket import aws_client_session


class TestAWSClientSession(unittest.TestCase):
    """
    Unitary tests for AWS Session Client.
    """

    HTTP_PROXY_IP_ADDRESS = "100.20.30.40:0123"
    get_logger_mock = mock.Mock()

    @classmethod
    def setUpClass(cls):
        """
        Global setUp
        """

        logging.basicConfig(level=logging.INFO)
        cls.aws_valid_key = r"\1234567890/ABcd"
        cls.aws_empty_key = ""
        cls.aws_invalid_key = 1234567890
        cls.valid_proxy = cls.HTTP_PROXY_IP_ADDRESS
        cls.empty_proxy = ""
        cls.invalid_proxy = [cls.HTTP_PROXY_IP_ADDRESS]

    @mock.patch('logging.getLogger', get_logger_mock)
    def setUp(self):
        """
        Test setUp
        """
        self.get_logger_mock.reset_mock()
        self.logger_mock = mock.Mock()
        self.get_logger_mock.return_value = self.logger_mock
        self.client_session_mock = mock.Mock()
        self.boto_session_mock = mock.Mock()
        self.aws_client = aws_client_session.AWSClient(
            aws_access_key=self.aws_valid_key,
            aws_secret_key=self.aws_valid_key)

    @mock.patch.object(aws_client_session.AWSClient, '_AWSClient__update_resource_configuration')
    @mock.patch("boto3.Session")
    def test_create_client_session(
            self, boto_session_mock, update_resource_configuration_mock):
        """
        This function tests 'aws_client_session.AWSClient.create_client_session' in the nominal
        and positive scenario.
        """
        self.aws_client._AWSClient__client_session = self.client_session_mock
        boto_session_mock.return_value = self.boto_session_mock
        self.aws_client.create_client_session()

        boto_session_mock.assert_called_once_with(
            aws_access_key_id=self.aws_valid_key,
            aws_secret_access_key=self.aws_valid_key)
        self.assertEquals(self.aws_client._AWSClient__client_session, self.boto_session_mock)
        update_resource_configuration_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Creating client session..."),
                mock.call("Client session has been created.")
            ]
        )

    @mock.patch("boto3.Session")
    def test_create_client_session_negative_case(self, boto_session_mock):
        """
        This function tests 'aws_client_session.AWSClient.create_client_session' in a negative scenario
        where the boto3.Session feature fails by any kind of error.
        """
        boto_session_mock.side_effect = [ValueError()]
        with self.assertRaises(aws_client_session.AWSCreationSessionError) as err:
            self.aws_client.create_client_session()
        self.assertEqual(str(err.exception), "Could not create a session to AWS service.")
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    @mock.patch.object(aws_client_session.AWSClient, 'create_client_session')
    def test_update_session_correct_keys(self, create_client_session_mock):
        """
        This function tests 'aws_client_session.AWSClient.update_session_keys' in the nominal and
        positive scenario. So it's using valid format keys.
        """
        self.aws_client.update_session_keys(
            aws_access_key=self.aws_valid_key,
            aws_secret_key=self.aws_valid_key)
        create_client_session_mock.assert_called_once()
        self.assertEquals(self.aws_client._AWSClient__aws_access_key, self.aws_valid_key)
        self.assertEquals(self.aws_client._AWSClient__aws_secret_key, self.aws_valid_key)

    @mock.patch.object(aws_client_session.AWSClient, 'create_client_session')
    def test_update_session_empty_keys(self, create_client_session_mock):
        """
        This function tests 'aws_client_session.AWSClient.update_session_keys' in the nominal and
        positive scenario. So it's using valid format but empty keys.
        """
        self.aws_client.update_session_keys(
            aws_access_key=self.aws_empty_key,
            aws_secret_key=self.aws_empty_key)
        create_client_session_mock.assert_called_once()
        self.assertEquals(self.aws_client._AWSClient__aws_access_key, self.aws_empty_key)
        self.assertEquals(self.aws_client._AWSClient__aws_secret_key, self.aws_empty_key)

    def test_update_session_invalid_keys(self):
        """
        This function tests 'aws_client_session.AWSClient.update_session_keys' in the negative and
        negative scenario where the keys are not in a valid format and makes the function fail.
        """
        with self.assertRaises(TypeError) as err:
            self.aws_client.update_session_keys(
                aws_access_key=self.aws_invalid_key,
                aws_secret_key=self.aws_invalid_key)
        self.assertEqual(str(err.exception), "Session keys should be strings!")

    @mock.patch.object(aws_client_session.AWSClient, '_AWSClient__update_resource_configuration')
    def test_update_proxy_valid_configuration(self, update_resource_configuration_mock):
        """
        This function tests 'aws_client_session.AWSClient.update_proxy_configuration' in the
        nominal and positive scenario. So it's using valid format proxy addresses.
        """
        self.aws_client.update_proxy_configuration(
            http_proxy=self.valid_proxy,
            https_proxy=self.valid_proxy)
        update_resource_configuration_mock.assert_called_once()
        self.assertEquals(self.aws_client._AWSClient__http_proxy, self.valid_proxy)
        self.assertEquals(self.aws_client._AWSClient__https_proxy, self.valid_proxy)

    @mock.patch.object(aws_client_session.AWSClient, '_AWSClient__update_resource_configuration')
    def test_update_proxy_empty_configuration(self, update_resource_configuration_mock):
        """
        This function tests 'aws_client_session.AWSClient.update_proxy_configuration' in the
        nominal and positive scenario. So it's using valid format but empty proxy addresses.
        """
        self.aws_client.update_proxy_configuration(
            http_proxy=self.empty_proxy,
            https_proxy=self.empty_proxy)
        update_resource_configuration_mock.assert_called_once()
        self.assertEquals(self.aws_client._AWSClient__http_proxy, self.empty_proxy)
        self.assertEquals(self.aws_client._AWSClient__https_proxy, self.empty_proxy)

    def test_update_proxy_invalid_configuration(self):
        """
        This function tests 'aws_client_session.AWSClient.update_proxy_configuration' in the
        negative scenario. So it's using invalid format proxy addresses.
        """
        with self.assertRaises(TypeError) as err:
            self.aws_client.update_proxy_configuration(
                http_proxy=self.invalid_proxy,
                https_proxy=self.invalid_proxy)
        self.assertEqual(str(err.exception), "Session proxy addresses should be strings!")

    def test_create_session_resource(self):
        """
        This function tests 'aws_client_session.AWSClient.__create_session_resource' in the
        nominal and positive scenario.
        """
        self.aws_client._AWSClient__client_session = self.client_session_mock
        session_resource_mock = mock.Mock()
        self.client_session_mock.resource.return_value = session_resource_mock
        self.aws_client._AWSClient__create_session_resource()

        self.client_session_mock.resource.assert_called_once_with(
            service_name=self.aws_client._S3_RESOURCE_NAME,
            verify=False
        )
        self.assertEquals(
            self.aws_client._AWSClient__client_session_resource,
            session_resource_mock)

    @mock.patch('botocore.config.Config')
    def test_update_resource_configuration(self, config_mock):
        """
        This function tests 'aws_client_session.AWSClient.__update_resource_configuration'
        in the nominal and positive scenario.
        """
        self.aws_client._AWSClient__http_proxy = self.valid_proxy
        self.aws_client._AWSClient__https_proxy = self.valid_proxy

        config_obj_mock = mock.Mock()
        config_mock.return_value = config_obj_mock

        self.aws_client._AWSClient__client_session = self.client_session_mock
        session_resource_mock = mock.Mock()
        self.client_session_mock.resource.return_value = session_resource_mock
        self.aws_client._AWSClient__update_resource_configuration()

        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Updating session resource configuration..."),
                mock.call("Session resource configuration has been updated.")
            ]
        )
        self.client_session_mock.resource.assert_called_once_with(
            service_name=self.aws_client._S3_RESOURCE_NAME,
            verify=False,
            config=config_obj_mock)
        config_mock.assert_called_once_with(
            proxies={
                "http": self.valid_proxy,
                "https": self.valid_proxy
            }
        )
        self.assertEquals(
            self.aws_client._AWSClient__client_session_resource,
            session_resource_mock)

    @mock.patch.object(aws_client_session.AWSClient, '_AWSClient__create_session_resource')
    def test_update_resource_empty_configuration(self, session_resource_mock):
        """
        This function tests 'aws_client_session.AWSClient.__update_resource_configuration'
        in a positive scenario. In this case are used empty proxy addresses so it's checking
        the 'else' part inside the 'try/except'.
        """
        self.aws_client._http_proxy = self.empty_proxy
        self.aws_client._https_proxy = self.empty_proxy
        self.aws_client._AWSClient__update_resource_configuration()
        session_resource_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Updating session resource configuration..."),
                mock.call("Session resource configuration has been updated.")
            ]
        )

    def test_update_resource_valid_configuration_negative_case(self):
        """
        This function tests 'aws_client_session.AWSClient.__update_resource_configuration'
        in a negative scenario. In this case are used valid proxy addresses so it's checking
        the 'if' part inside the 'try/except'.
        """
        self.aws_client._AWSClient__http_proxy = self.valid_proxy
        self.aws_client._AWSClient__https_proxy = self.valid_proxy

        self.aws_client._AWSClient__client_session = self.client_session_mock
        self.client_session_mock.resource.side_effect = [ValueError()]
        with self.assertRaises(aws_client_session.AWSResourceConfigurationError) as err:
            self.aws_client._AWSClient__update_resource_configuration()
        self.assertEqual(
            str(err.exception),
            "Could not update properly the session resource configuration.")
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    @mock.patch.object(aws_client_session.AWSClient, '_AWSClient__create_session_resource')
    def test_update_resource_empty_configuration_negative_case(self, session_resource_mock):
        """
        This function tests 'aws_client_session.AWSClient.__update_resource_configuration'
        in a negative scenario. In this case are used empty proxy addresses so it's checking
        the 'else' part inside the 'try/except'.
        """
        self.aws_client._AWSClient__http_proxy = self.empty_proxy
        self.aws_client._AWSClient__https_proxy = self.empty_proxy

        self.aws_client._AWSClient__client_session = self.client_session_mock
        session_resource_mock.side_effect = [ValueError()]
        with self.assertRaises(aws_client_session.AWSResourceConfigurationError) as err:
            self.aws_client._AWSClient__update_resource_configuration()
        self.assertEqual(str(err.exception),
                         "Could not update properly the session resource configuration.")
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")
