"""
Unitary testing for AWS Bucket Session module.

:author: Xavi


:date: 2018-10-22
"""

import logging
import mock
import unittest

from awsbucket import aws_bucket


class TestAWSBucket(unittest.TestCase):

    HTTP_PROXY_IP_ADDRESS = "100.20.30.40:0123"
    get_logger_mock = mock.Mock()

    @classmethod
    def setUpClass(cls):
        """
        Global setUp
        """

        logging.basicConfig(level=logging.INFO)
        cls.bucket_name = "Bucket1"
        cls.bucket2_name = "Bucket2"
        cls.valid_key = "0123456789ABcd"
        cls.valid_key_2 = "Abcd9876543210"
        cls.empty_key = ""
        cls.invalid_key = 123456789
        cls.valid_proxy = cls.HTTP_PROXY_IP_ADDRESS
        cls.empty_proxy = ""
        cls.valid_delete_dict = {'Objects':
                                 [
                                     {
                                         'Key': cls.valid_key,
                                     }
                                 ]}
        cls.empty_delete_dict = {}
        cls.valid_filter = r"\ABcd/123"
        cls.invalid_filter = [cls.valid_filter]

    @mock.patch('logging.getLogger', get_logger_mock)
    def setUp(self):
        """
        Test setUp
        """
        self.get_logger_mock.reset_mock()
        self.logger_mock = mock.Mock()
        self.get_logger_mock.return_value = self.logger_mock
        self.client_session_mock = mock.Mock()
        self.bucket_session_mock = mock.Mock()
        self.bucket = aws_bucket.AWSBucket()

    def tearDown(self):
        """
        Test tearDown
        """
        self.client_session_mock = mock.Mock()
        self.bucket_session_mock = mock.Mock()

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__create_session')
    def test_check_existing_session(self, create_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.__check_existing_session' in a nominal and
        positive scenario. In this case is checked that there was already an existing session
        (self.__session_exists = True).
        """
        self.bucket._AWSBucket__session_exists = True
        self.bucket._AWSBucket__check_existing_session()
        create_session_mock.assert_not_called()
        self.assertEquals(self.bucket._AWSBucket__session_exists, True)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__create_session')
    def test_check_no_existing_session(self, create_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.__check_existing_session' in a nominal and
        positive scenario. In this case is checked that there was no existing session
        (self.__session_exists = True).
        """
        self.bucket._AWSBucket__session_exists = False
        self.bucket._AWSBucket__check_existing_session()
        create_session_mock.assert_called_once()
        self.assertEquals(self.bucket._AWSBucket__session_exists, True)

    def test_check_bucket_connection(self):
        """
        This function tests 'aws_bucket.AWSBucket.__check_bucket_connection' in a nominal and
        positive scenario. In this case is checked that there was already an existing connection
        to an existing AWS S3 Bucket (self.__bucket = True).
        """
        self.bucket._AWSBucket__bucket = True
        self.bucket._AWSBucket__check_bucket_connection()

    def test_check_no_bucket_connection(self):
        """
        This function tests 'aws_bucket.AWSBucket.__check_bucket_connection' in a nominal and
        positive scenario. In this case is checked that there was no existing connection
        to an existing AWS S3 Bucket (self.__bucket = False).
        """
        self.bucket._AWSBucket__bucket = False
        with self.assertRaises(aws_bucket.AWSBucketNotConnectedError) as err:
            self.bucket._AWSBucket__check_bucket_connection()
        self.assertEqual(str(err.exception), "No connection established with any AWS Bucket. "
                                             "Please, connect to a AWS Bucket previously.")

    @mock.patch("awsbucket.aws_client_session.AWSClient")
    def test_create_session(self, client_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.__create_session' in a nominal and positive
        scenario.
        """
        self.bucket._AWSBucket__client_session = self.client_session_mock
        client_session_mock.return_value = self.client_session_mock

        session_property = mock.Mock()
        self.client_session_mock.session = session_property

        self.bucket._AWSBucket__create_session()
        client_session_mock.assert_called_once()
        self.assertEquals(self.bucket._AWSBucket__client_session,
                          self.client_session_mock)
        self.assertEquals(self.bucket._AWSBucket__bucket_session,
                          session_property)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_existing_session')
    def test_set_session_keys(self, existing_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.set_session_keys' in a nominal and
        positive scenario.
        """
        self.bucket._AWSBucket__client_session = self.client_session_mock

        session_property = mock.Mock()
        self.client_session_mock.session = session_property

        self.bucket.set_session_keys(
            access_key=self.valid_key,
            secret_key=self.empty_key)

        existing_session_mock.assert_called_once()
        self.client_session_mock.update_session_keys.assert_called_once_with(
            aws_access_key=self.valid_key,
            aws_secret_key=self.empty_key)
        self.assertEquals(self.bucket._AWSBucket__bucket_session,
                          session_property)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_existing_session')
    def test_configure_proxy_settings(self, existing_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.configure_proxy_settings' in a nominal and
        positive scenario.
        """
        self.bucket._AWSBucket__client_session = self.client_session_mock

        session_property = mock.Mock()
        self.client_session_mock.session = session_property

        self.bucket.configure_proxy_settings(
            http_proxy=self.valid_proxy,
            https_proxy=self.valid_proxy)

        existing_session_mock.assert_called_once()
        self.client_session_mock.update_proxy_configuration.assert_called_once_with(
            http_proxy=self.valid_proxy,
            https_proxy=self.valid_proxy)
        self.assertEquals(self.bucket._AWSBucket__bucket_session,
                          session_property)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_existing_session')
    def test_configure_proxy_settings_connection_already_established(
            self, existing_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.configure_proxy_settings' in a nominal and
        positive scenario but in this case is checked the if statement performance.
        """
        self.bucket._AWSBucket__client_session = self.client_session_mock
        # Mock and simulate parameters and functions in order to check the IF statement
        self.bucket._AWSBucket__bucket_name = self.bucket_name
        self.bucket._AWSBucket__connection_already_established = True
        self.bucket.connect_to_bucket = mock.Mock()

        session_property = mock.Mock()
        self.client_session_mock.session = session_property

        self.bucket.configure_proxy_settings(
            http_proxy=self.valid_proxy,
            https_proxy=self.valid_proxy)

        existing_session_mock.assert_called_once()
        self.client_session_mock.update_proxy_configuration.assert_called_once_with(
            http_proxy=self.valid_proxy,
            https_proxy=self.valid_proxy
        )
        self.assertEquals(
            self.bucket._AWSBucket__bucket_session,
            session_property
        )
        self.bucket.connect_to_bucket.assert_called_once_with(
            bucket_name=self.bucket_name
        )

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_existing_session')
    def test_connect_to_bucket(self, existing_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.connect_to_bucket' in a nominal and
        positive scenario, so the connection to the Bucket is established correctly.
        """
        self.bucket._AWSBucket__bucket_session = self.bucket_session_mock

        bucket_connection_mock = mock.Mock()
        self.bucket_session_mock.Bucket.return_value = bucket_connection_mock

        self.bucket.connect_to_bucket(self.bucket_name)
        self.assertEquals(self.bucket._AWSBucket__bucket_name,
                          self.bucket_name)
        existing_session_mock.assert_called_once()

        self.bucket_session_mock.Bucket.assert_called_once_with(
            self.bucket_name
        )
        self.assertEquals(
            self.bucket._AWSBucket__bucket,
            bucket_connection_mock
        )
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Connecting to Bucket: '" + self.bucket_name + "'"),
                mock.call("Successfully connected to the AWS Bucket.")
            ]
        )
        self.assertEquals(self.bucket._AWSBucket__connection_already_established, True)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_existing_session')
    def test_cant_connect_to_bucket(self, existing_session_mock):
        """
        This function tests 'aws_bucket.AWSBucket.connect_to_bucket' in a negative
        scenario, so the connection to the Bucket can't be established properly.
        """
        self.bucket._AWSBucket__bucket_session = self.bucket_session_mock
        self.bucket_session_mock.Bucket.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSBucketConnectionError) as err:
            self.bucket.connect_to_bucket(self.bucket_name)
        self.assertEqual(
            str(err.exception),
            "Could establish connection with the '" + self.bucket_name + "' AWS Bucket."
        )
        self.logger_mock.info.assert_called_once_with(
            "Connecting to Bucket: '" +
            self.bucket_name + "'"
        )
        self.assertEquals(
            self.bucket._AWSBucket__bucket_name,
            self.bucket_name
        )
        existing_session_mock.assert_called_once()
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_copy_file_from_source(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.copy_file_from_source' in a positive
        scenario, so file has been copied correctly to the target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket.copy
        copy_to_bucket_mock = mock.Mock()
        self.bucket_session_mock.copy.return_value = copy_to_bucket_mock

        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        self.bucket._AWSBucket__bucket_name = self.bucket_name
        self.bucket.copy_file_from_source(
            origin_bucket_name=self.bucket2_name,
            origin_key_object=self.valid_key,
            destination_key=self.valid_key_2,
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Copying file from '" + self.valid_key +
                          "' origin AWS Bucket to target AWS Bucket: '" +
                          self.valid_key_2 + "'"),
                mock.call("File has been copied successfully to AWS Bucket: '" +
                          self.valid_key_2 + "'")
            ]
        )

        self.bucket_session_mock.copy.assert_called_once_with(
            CopySource={
                "Bucket": self.bucket2_name,
                "Key": self.valid_key
            },
            Key=self.valid_key_2)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_copy_file_from_source(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.copy_file_from_source' in a negative
        scenario, so an unexpected error occurs while copying the desired file to the
        target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        # Add a raising exception side effect to self.__bucket.copy
        self.bucket_session_mock.copy.side_effect = [ValueError()]

        self.bucket._AWSBucket__bucket_name = self.bucket_name

        with self.assertRaises(aws_bucket.AWSCopyToBucketError) as err:
            self.bucket.copy_file_from_source(
                origin_bucket_name=self.bucket2_name,
                origin_key_object=self.valid_key,
                destination_key=self.valid_key_2,
            )
            self.assertEqual(
                str(err.exception),
                "Something went wrong copying file to bucket destination."
                "Please, check origin and destination paths."
            )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_called_once_with(
            "Copying file from '" + self.valid_key +
            "' origin AWS Bucket to target AWS Bucket: '" + self.valid_key_2 + "'")
        self.bucket_session_mock.copy.assert_called_once_with(
            CopySource={
                "Bucket": self.bucket2_name,
                "Key": self.valid_key
            },
            Key=self.valid_key_2)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_delete_file_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.delete_file_from_bucket' in a positive
        scenario, so file has been deleted correctly from the target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket.delete_objects
        copy_to_bucket_mock = mock.Mock()
        self.bucket_session_mock.delete_objects.return_value = copy_to_bucket_mock

        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        self.bucket._AWSBucket__bucket_name = self.bucket_name

        self.bucket.delete_file_from_bucket(delete=self.valid_delete_dict)

        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Deleting following objects from AWS S3 Bucket:"),
                mock.call("    Key: '" + self.valid_key + "'"),
                mock.call("Deletion has been successful.")
            ]
        )
        self.bucket_session_mock.delete_objects.assert_called_once_with(
            Delete=self.valid_delete_dict)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_delete_file_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.delete_file_from_bucket' in a negative
        scenario, so an unexpected error occurs while deleting the desired file to the
        target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        # Add a raising exception side effect to self.__bucket.delete_objects
        self.bucket_session_mock.delete_objects.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSDeleteObjectsFromBucketError) as err:
            self.bucket.delete_file_from_bucket(delete=self.valid_delete_dict)
        self.assertEqual(
            str(err.exception),
            "Something went wrong deleting file on bucket destination."
            "Please, check deletion input dictionary file."
        )
        existing_bucket_mock.assert_called_once()
        self.bucket_session_mock.delete_objects.assert_called_once_with(
            Delete=self.valid_delete_dict)

        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Deleting following objects from AWS S3 Bucket:"),
                mock.call("    Key: '" + self.valid_key + "'"),
             ]
        )
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    @mock.patch.object(aws_bucket.AWSBucket, 'delete_file_from_bucket')
    @mock.patch.object(aws_bucket.AWSBucket, 'copy_file_from_source')
    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_move_file_from_bucket(
            self, existing_bucket_mock, copy_function_mock, delete_file_mock):
        """
        This function tests 'aws_bucket.AWSBucket.move_file_from_bucket' in a positive
        scenario, so file has been copied and moved correctly from a AWS S3 origin Bucket
        to the target AWS Bucket.
        """
        delete_function_mock_return = mock.Mock()
        delete_file_mock.return_value = delete_function_mock_return
        result = self.bucket.move_file_from_bucket(
            origin_bucket_name=self.bucket2_name,
            origin_key_object=self.valid_key,
            destination_key=self.valid_key
        )
        existing_bucket_mock.assert_called_once()
        copy_function_mock.assert_called_with(
            origin_bucket_name=self.bucket2_name,
            origin_key_object=self.valid_key,
            destination_key=self.valid_key
        )
        delete_dict = {
            'Objects':
                [
                    {'Key': self.valid_key}
                ]
        }
        delete_file_mock.assert_called_with(delete=delete_dict)
        self.assertEquals(result, delete_function_mock_return)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_download_file_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.download_file_from_bucket' in a positive
        scenario, so file has been downloaded correctly from the already connected AWS Bucket.
        """
        download_from_bucket_mock = mock.Mock()
        self.bucket_session_mock.download_file.return_value = download_from_bucket_mock
        self.bucket._AWSBucket__bucket = self.bucket_session_mock

        self.bucket.download_file_from_bucket(
            file_key=self.valid_key,
            destination_path=self.valid_key_2
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Downloading file from AWS S3 Bucket in: '" +
                          self.valid_key + "', on path: '" + self.valid_key_2 + "'"),
                mock.call("Download process has finished."),
             ]
        )

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_download_file_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.download_file_from_bucket' in a negative
        scenario, so an unexpected error occurs while download the desired file from the
        target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        # Add a raising exception side effect to self.__bucket.download_file
        self.bucket_session_mock.download_file.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSDownloadObjectFromBucketError) as err:
            self.bucket.download_file_from_bucket(
                file_key=self.valid_key,
                destination_path=self.valid_key_2
            )
        self.assertEqual(
            str(err.exception),
            "Something went wrong downloading file from AWS S3 Bucket."
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")
        self.logger_mock.info.assert_called_once_with(
            "Downloading file from AWS S3 Bucket in: '" + self.valid_key + "', "
            "on path: '" + self.valid_key_2 + "'"
        )

    def test_download_file_from_bucket_invalid_keys(self):
        """
        This function tests 'aws_bucket.AWSBucket.download_file_from_bucket' in a negative
        scenario, so the used keys are provided in an invalid format so the function fails
        at the beginnig.
        """
        with self.assertRaises(TypeError) as err:
            self.bucket.download_file_from_bucket(
                file_key=self.invalid_key,
                destination_path=self.invalid_key
            )
        self.assertEqual(
            str(err.exception),
            "Both bucket object target key or destination path shall be in"
            "string format."
        )

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_upload_file_to_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.upload_file_to_bucket' in a positive
        scenario, so file has been uploaded correctly to a target AWS S3 Bucket key.
        """
        upload_to_bucket_mock = mock.Mock()
        self.bucket_session_mock.upload_file.return_value = upload_to_bucket_mock
        self.bucket._AWSBucket__bucket = self.bucket_session_mock

        self.bucket.upload_file_to_bucket(
            file_to_upload=self.valid_key,
            target_key=self.valid_key
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Uploading file to AWS S3 Bucket on path: '" + self.valid_key + "'"),
                mock.call("Upload process has finished."),
             ]
        )

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_upload_file_to_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.upload_file_to_bucket' in a negative
        scenario, so an unexpected error occurs while uploading the desired file to the
        target AWS Bucket key.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        # Add a raising exception side effect to self.__bucket.download_file
        self.bucket_session_mock.upload_file.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSUploadObjectFromBucketError) as err:
            self.bucket.upload_file_to_bucket(
                file_to_upload=self.valid_key,
                target_key=self.valid_key
            )
        self.assertEqual(
            str(err.exception),
            "Something went wrong uploading file to AWS S3 Bucket."
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")
        self.logger_mock.info.assert_called_once_with(
            "Uploading file to AWS S3 Bucket on path: '" + self.valid_key + "'")

    def test_upload_file_to_bucket_invalid_keys(self):
        """
        This function tests 'aws_bucket.AWSBucket.upload_file_to_bucket' in a negative
        scenario, so the input keys are provided in an invalid format so the function
        shall fail at the very beginning.
        """
        with self.assertRaises(TypeError) as err:
            self.bucket.upload_file_to_bucket(
                file_to_upload=self.invalid_key,
                target_key=self.invalid_key
            )
        self.assertEqual(
            str(err.exception),
            r"Both object path/key to upload or target path inside Bucket "
            "shall be in string format."
        )

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_summary_all_objects_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.summary_all_objects_from_bucket' in
        a positive scenario, so all object keys inside the AWS S3 Bucket have been
        retrieved correctly in a list format.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        objects_all_mock = mock.MagicMock()
        self.bucket_session_mock.objects.all.return_value = objects_all_mock

        result = self.bucket.summary_all_objects_from_bucket()
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_called_once_with(
            "Getting all objects existing in the AWS S3 Bucket."
        )
        self.bucket_session_mock.objects.all.assert_called_once()
        self.assertIsInstance(result, list)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_summarize_all_objects_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.summary_all_objects_from_bucket' in a
        negative scenario, so an unexpected error occurs while trying to summarize the
        all objects from the already connected AWS S3 Bucket.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        self.bucket_session_mock.objects.all.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSBucketConnectionError) as err:
            self.bucket.summary_all_objects_from_bucket()
        self.assertEqual(
            str(err.exception),
            "Something went wrong during the process. Please, retry this "
            "process again or check the Bucket's connection."
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_called_once_with(
            "Getting all objects existing in the AWS S3 Bucket.")
        self.bucket_session_mock.objects.all.assert_called_once()
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_filter_objects_by_key_on_bucket_with_delimiter(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.filter_objects_by_key_on_bucket' in
        a positive scenario, so all object keys inside the AWS S3 Bucket have been
        filtered and retrieved correctly in a list format.
        """

        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        objects_all_mock = mock.MagicMock()
        self.bucket_session_mock.objects.filter.return_value = objects_all_mock

        result = self.bucket.filter_objects_by_key_on_bucket(
            prefix_filter=self.valid_filter,
            delimiter_subfolders=True
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Getting all objects existing in the AWS S3 Bucket that their "
                          "key starts with: '" + self.valid_filter + "'"),
                mock.call("Filtered objects: ")
             ]
        )
        self.bucket_session_mock.objects.filter.assert_called_once_with(
            Prefix=self.valid_filter,
            Delimiter="/"
        )
        self.assertIsInstance(result, list)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_filter_objects_by_key_on_bucket_without_delimiter(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.filter_objects_by_key_on_bucket' in
        a positive scenario, so all object keys inside the AWS S3 Bucket have been
        filtered and retrieved correctly in a list format.
        """

        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        objects_all_mock = mock.MagicMock()
        self.bucket_session_mock.objects.filter.return_value = objects_all_mock

        result = self.bucket.filter_objects_by_key_on_bucket(
            prefix_filter=self.valid_filter,
            delimiter_subfolders=False
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_has_calls(
            [
                mock.call("Getting all objects existing in the AWS S3 Bucket that their "
                          "key starts with: '" + self.valid_filter + "'"),
                mock.call("Filtered objects: ")
            ]
        )
        self.bucket_session_mock.objects.filter.assert_called_once_with(
            Prefix=self.valid_filter,
        )
        self.assertIsInstance(result, list)

    @mock.patch.object(aws_bucket.AWSBucket, '_AWSBucket__check_bucket_connection')
    def test_cant_filter_all_objects_from_bucket(self, existing_bucket_mock):
        """
        This function tests 'aws_bucket.AWSBucket.filter_objects_by_key_on_bucket' in a
        negative scenario, so an unexpected error occurs while trying to filter from all
        existing objects placed in the already connected AWS S3 Bucket.
        """
        # MOCK DEFINITION FOR: self.__bucket
        self.bucket._AWSBucket__bucket = self.bucket_session_mock
        self.bucket_session_mock.objects.filter.side_effect = [ValueError()]

        with self.assertRaises(aws_bucket.AWSBucketConnectionError) as err:
            self.bucket.filter_objects_by_key_on_bucket(prefix_filter=self.valid_filter)
        self.assertEqual(
            str(err.exception),
            "Something went wrong during the process. Please, retry this "
            "process again, check the input filter_prefix or check the "
            "Bucket's connection."
        )
        existing_bucket_mock.assert_called_once()
        self.logger_mock.info.assert_called_once_with(
            "Getting all objects existing in the AWS S3 Bucket that their "
            "key starts with: '" + self.valid_filter + "'"
        )
        self.bucket_session_mock.objects.filter.assert_called_once()
        self.logger_mock.exception.assert_called_once_with("Unexpected Exception: ")

    def test_filter_objects_by_key_on_bucket_with_invalid_filter(self):
        """
        This function tests 'aws_bucket.AWSBucket.filter_objects_by_key_on_bucket' in a
        negative scenario, so the input arguments are provided in an invalid format so the
        function shall fail at the very beginning.
        """
        with self.assertRaises(TypeError) as err:
            self.bucket.filter_objects_by_key_on_bucket(
                prefix_filter=self.invalid_filter)
        self.assertEqual(
            str(err.exception),
            "The used prefix filter entered is not an string. "
            "Please, check the input format."
        )
